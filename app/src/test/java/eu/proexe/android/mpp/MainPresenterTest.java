package eu.proexe.android.mpp;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import eu.proexe.android.mpp.data.DataManager;
import eu.proexe.android.mpp.ui.main.MainMvpView;
import eu.proexe.android.mpp.ui.main.MainPresenter;
import eu.proexe.android.mpp.util.RxSchedulersOverrideRule;

@RunWith(MockitoJUnitRunner.class)
public class MainPresenterTest {

    @Rule
    public final RxSchedulersOverrideRule mOverrideSchedulersRule = new RxSchedulersOverrideRule();
    @Mock
    MainMvpView mMockMainMvpView;
    @Mock
    DataManager mMockDataManager;

    private MainPresenter mMainPresenter;

    @Before
    public void setUp() {
//        mMainPresenter = new MainPresenter(mMockDataManager);
//        mMainPresenter.attachView(mMockMainMvpView);
    }

    @After
    public void tearDown() {
//        mMainPresenter.detachView();
    }


    @Test
    public void empty() {

    }

}