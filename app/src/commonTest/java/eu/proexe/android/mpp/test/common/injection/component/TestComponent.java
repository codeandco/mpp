package eu.proexe.android.mpp.test.common.injection.component;

import javax.inject.Singleton;

import dagger.Component;
import eu.proexe.android.mpp.injection.component.ApplicationComponent;
import eu.proexe.android.mpp.test.common.injection.module.ApiTestModule;
import eu.proexe.android.mpp.test.common.injection.module.ApplicationTestModule;

@Singleton
@Component(modules = {ApplicationTestModule.class, ApiTestModule.class})
public interface TestComponent extends ApplicationComponent {

}
