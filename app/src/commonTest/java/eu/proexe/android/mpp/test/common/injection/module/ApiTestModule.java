package eu.proexe.android.mpp.test.common.injection.module;

import dagger.Module;
import dagger.Provides;
import eu.proexe.android.mpp.data.remote.ApiService;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

import static org.mockito.Mockito.mock;

/**
 * Created by Maciej Różański on 07.12.2016.
 */

@Module
public class ApiTestModule {

    @Provides
    OkHttpClient provideOkHttpClient() {
        return mock(OkHttpClient.class);
    }

    @Provides
    Retrofit provideRetrofit(OkHttpClient client) {
        return mock(Retrofit.class);
    }

    @Provides
    ApiService provideApiService(Retrofit retrofit) {
        return mock(ApiService.class);
    }
}
