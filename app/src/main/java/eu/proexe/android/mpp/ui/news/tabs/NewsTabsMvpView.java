package eu.proexe.android.mpp.ui.news.tabs;

import java.util.List;

import eu.proexe.android.mpp.data.model.response.category.NewsCategory;
import eu.proexe.android.mpp.ui.base.LoadingMvpView;


public interface NewsTabsMvpView extends LoadingMvpView<List<NewsCategory>> {
    void showTabWithName(NewsCategory category);
}
