package eu.proexe.android.mpp.ui.news.list;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import eu.proexe.android.mpp.R;
import eu.proexe.android.mpp.data.model.response.Article;
import eu.proexe.android.mpp.data.model.response.category.NewsCategory;
import eu.proexe.android.mpp.ui.base.BaseActivity;
import eu.proexe.android.mpp.ui.news.details.NewsDetailsActivity;
import eu.proexe.android.mpp.util.VerticalSpaceItemDecoration;
import eu.proexe.android.mpp.util.ViewUtil;
import rx.Observable;

public class NewsListFragment extends Fragment implements NewsListMvpView {


    private static final String EXTRA_CATEGORY_ID = "EXTRA_CATEGORY_ID";
    @Inject
    NewsListPresenter presenter;
    @BindView(R.id.list)
    RecyclerView list;
    @Inject
    NewsAdapter newsAdapter;
    @BindView(R.id.progress)
    View progress;
    private Unbinder bind;

    public static NewsListFragment newInstance(NewsCategory newsCategory) {
        NewsListFragment fragment = new NewsListFragment();
        Bundle args = new Bundle();
        args.putString(EXTRA_CATEGORY_ID, newsCategory.NewsTypeID);
        fragment.setArguments(args);
        return fragment;
    }

    String getNewsTypeIdExtra() {
        return getArguments().getString(EXTRA_CATEGORY_ID);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflated = inflater.inflate(R.layout.fragment_news_list, container, false);
        bind = ButterKnife.bind(this, inflated);
        return inflated;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        list.setLayoutManager(new LinearLayoutManager(getActivity()));
        list.setAdapter(newsAdapter);
        list.addItemDecoration(new VerticalSpaceItemDecoration(ViewUtil.dpToPx(2)));

        presenter.attachView(this);
        presenter.loadArticles(getNewsTypeIdExtra());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((BaseActivity) getActivity()).activityComponent().inject(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.detachView();
        bind.unbind();
    }

    @Override
    public void showLoading(boolean show) {
        progress.animate().alpha(show ? 1 : 0).setDuration(250);
    }

    @Override
    public void showErrorLoading() {
    }

    @Override
    public void showItems(List<Article> data) {
        newsAdapter.setItems(data, getNewsTypeIdExtra());
    }

    @Override
    public void showEmpty() {
    }

    @Override
    public void hideEmpty() {
    }

    @Override
    public Observable<Article> articleClicked() {
        return newsAdapter.articleClicked();
    }

    @Override
    public void showArticleDetail(Article article) {
        NewsDetailsActivity.start(getActivity(), article, getNewsTypeIdExtra(), "", false);
    }

    @Override
    public void setTextSize(float textSize) {
        newsAdapter.setTextSize(textSize);
    }
}
