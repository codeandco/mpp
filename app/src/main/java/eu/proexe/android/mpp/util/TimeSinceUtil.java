package eu.proexe.android.mpp.util;

import android.content.Context;

import org.joda.time.DateTime;
import org.joda.time.Months;
import org.joda.time.YearMonth;

import eu.proexe.android.mpp.R;


public class TimeSinceUtil {
    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;


    public static String getTimeAgo(long time, Context ctx) {
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        long now = getCurrentTime();
        if (time > now || time <= 0) {
            return null;
        }


        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return ctx.getString(R.string.timesince_justnow);
        } else if (diff < 2 * MINUTE_MILLIS) {
            return ctx.getString(R.string.timesince_minuteago);
        } else if (diff < 50 * MINUTE_MILLIS) {
            return ctx.getString(R.string.time_minutes,
                    NumbersUtils.format((int) (diff / MINUTE_MILLIS)));
        } else if (diff < 90 * MINUTE_MILLIS) {
            return ctx.getString(R.string.timesince_hourago);
        } else if (diff < 24 * HOUR_MILLIS) {
            return ctx.getString(R.string.time_hours,
                    NumbersUtils.format((int) (diff / HOUR_MILLIS)));
        } else if (diff < 48 * HOUR_MILLIS) {
            return ctx.getString(R.string.timesince_yesterday);
        } else {
            return ctx.getString(R.string.time_days,
                    NumbersUtils.format((int) (diff / DAY_MILLIS)));
        }
    }

    private static long getCurrentTime() {
        return System.currentTimeMillis();
    }


    private static YearMonth toYearMonth(DateTime date) {
        return new YearMonth(date.getYear(), date.getMonthOfYear());
    }

    private static int monthsBetween(DateTime start, DateTime end) {
        return Months.monthsBetween(toYearMonth(start), toYearMonth(end)).getMonths();
    }
}