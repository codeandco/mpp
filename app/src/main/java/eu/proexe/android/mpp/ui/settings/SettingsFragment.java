package eu.proexe.android.mpp.ui.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import eu.proexe.android.mpp.R;
import eu.proexe.android.mpp.ui.base.BaseActivity;
import eu.proexe.android.mpp.ui.settings.about.AboutFragment;
import eu.proexe.android.mpp.ui.settings.notifications_2.NotiFragment;
import eu.proexe.android.mpp.ui.settings.privacy.PrivacyPolicyFragment;
import eu.proexe.android.mpp.ui.settings.signin.SignInFragment;
import eu.proexe.android.mpp.ui.settings.terms.TermsFragment;
import eu.proexe.android.mpp.ui.settings.text.size.TextSizeSettingsFragment;
import eu.proexe.android.mpp.util.StylingUtils;

/**
 * Created by Michał Trętowicz  on 09.02.17.
 */

public class SettingsFragment extends Fragment implements SettingsMvpView {

    @Inject
    SettingsPresenter presenter;

    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.pager)
    ViewPager pager;
    private Unbinder bind;
    private TabLayout.OnTabSelectedListener listener;

    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        fragment.setArguments(new Bundle());
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflated = inflater.inflate(R.layout.fragment_settings, container, false);
        bind = ButterKnife.bind(this, inflated);

        Fragment[] fragments = {
                new NotiFragment(),
                new TextSizeSettingsFragment(),
                new SignInFragment(),
                new AboutFragment(),
                new TermsFragment(),
                new PrivacyPolicyFragment()
        };

        String[] titles = getResources().getStringArray(R.array.settings);

        pager.setAdapter(new Adapter(getChildFragmentManager(), fragments, titles));
        tabs.setupWithViewPager(pager);


        listener = StylingUtils.styleTabs(pager, tabs,true,12, null);
        tabs.addOnTabSelectedListener(listener);


        pager.setOffscreenPageLimit(8);

        return inflated;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.attachView(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(listener != null) {
            tabs.removeOnTabSelectedListener(listener);
        }
        presenter.detachView();
        bind.unbind();
    }

    @Override
    public void setTextSize(float textSize) {

        if(listener != null) {
            tabs.removeOnTabSelectedListener(listener);
        }
        listener = StylingUtils.restyle(pager, tabs,true,textSize, null);
        tabs.addOnTabSelectedListener(listener);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((BaseActivity) getActivity()).activityComponent().inject(this);
    }



    private static class Adapter extends FragmentStatePagerAdapter {

        private final String[] titles;
        Fragment[] fragments;

        Adapter(FragmentManager fm, Fragment[] fragments, String[] titles) {
            super(fm);
            this.fragments = fragments;
            this.titles = titles;
        }

        @Override
        public Fragment getItem(int position) {
            return fragments[position];
        }

        @Override
        public int getCount() {
            return fragments.length;
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }
    }
}
