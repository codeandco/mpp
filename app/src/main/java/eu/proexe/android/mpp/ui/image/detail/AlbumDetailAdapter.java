package eu.proexe.android.mpp.ui.image.detail;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import eu.proexe.android.mpp.R;
import eu.proexe.android.mpp.data.ImageLoader;
import eu.proexe.android.mpp.data.model.response.album.AlbumDetailItem;
import eu.proexe.android.mpp.injection.ActivityContext;

/**
 * Created by Michał Trętowicz  on 20.02.17.
 */

public class AlbumDetailAdapter extends PagerAdapter {

    private List<AlbumDetailItem> items = new ArrayList<>();
    private Context mContext;
    private final ImageLoader imageLoader;

    @Inject
    public AlbumDetailAdapter(@ActivityContext Context context, ImageLoader imageLoader) {
        mContext = context;
        this.imageLoader = imageLoader;
    }

    public void setItems(List<AlbumDetailItem> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View layout = inflater.inflate(R.layout.item_album_detail, collection, false);
        collection.addView(layout);

        ImageView image = ButterKnife.findById(layout, R.id.image);


        imageLoader.loadFit(image, items.get(position).imageFullPath);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
