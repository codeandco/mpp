package eu.proexe.android.mpp.ui.image;

public interface OnBackPressedFragment {
    boolean onBackPressed();
}