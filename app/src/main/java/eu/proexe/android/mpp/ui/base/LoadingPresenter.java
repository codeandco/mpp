package eu.proexe.android.mpp.ui.base;


import java.util.List;

import eu.proexe.android.mpp.data.DataManager;
import eu.proexe.android.mpp.util.RxEventBus;
import eu.proexe.android.mpp.util.RxUtil;
import rx.Subscriber;
import rx.Subscription;


public class LoadingPresenter<T extends LoadingMvpView> extends BasePresenter<T> {

    private Subscription refreshSub;

    public LoadingPresenter(DataManager mDataManager,
                            RxEventBus mRxEventBus) {
        super(mDataManager, mRxEventBus);
    }

    @Override
    public void attachView(T mvpView) {
        super.attachView(mvpView);
        RxUtil.unsubscribe(refreshSub);
    }

    public void refresh() {
    }

    @Override
    public void detachView() {
        super.detachView();
        RxUtil.unsubscribe(refreshSub);
    }


    public class LoadingSubscriber<T extends List<?>> extends Subscriber<T> {

        @Override
        public void onCompleted() {
        }

        @Override
        public void onError(Throwable e) {
            getMvpView().showErrorLoading();
            getMvpView().showLoading(false);
        }

        @Override
        public void onNext(T orders) {
            getMvpView().showLoading(false);
            if (orders.isEmpty()) {
                getMvpView().showEmpty();
            } else {
                getMvpView().showItems(orders);
            }
        }

        @Override
        public void onStart() {
            getMvpView().showLoading(true);
        }
    }


    public class SingleValueLoadingSubscriber<T> extends Subscriber<T> {

        @Override
        public void onCompleted() {
        }

        @Override
        public void onError(Throwable e) {
            getMvpView().showErrorLoading();
            getMvpView().showLoading(false);
        }

        @Override
        public void onNext(T orders) {
            getMvpView().showLoading(false);
            if (orders == null) {
                getMvpView().showEmpty();
            } else {
                getMvpView().showItems(orders);
            }
        }

        @Override
        public void onStart() {
            getMvpView().showLoading(true);
        }
    }

}
