package eu.proexe.android.mpp.ui.settings.signin;

import javax.inject.Inject;

import eu.proexe.android.mpp.data.DataManager;
import eu.proexe.android.mpp.data.event.OnActivityResultEvent;
import eu.proexe.android.mpp.ui.base.BasePresenter;
import eu.proexe.android.mpp.util.RxEventBus;
import eu.proexe.android.mpp.util.RxUtil;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Michał Trętowicz  on 12.03.17.
 */

public class SignInPresenter extends BasePresenter<SignInMvpView> {

    private Subscription twitterSub;

    @Inject
    public SignInPresenter(DataManager mDataManager, RxEventBus mRxEventBus) {
        super(mDataManager, mRxEventBus);
    }

    @Override
    public void attachView(SignInMvpView mvpView) {
        super.attachView(mvpView);

        RxUtil.unsubscribe(twitterSub);
        twitterSub = mRxEventBus.filteredObservable(OnActivityResultEvent.class)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<OnActivityResultEvent>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(OnActivityResultEvent onActivityResultEvent) {
                        getMvpView().checkTwitterLoginResult(onActivityResultEvent);
                    }
                });
    }

    @Override
    public void detachView() {
        super.detachView();
        RxUtil.unsubscribe(twitterSub);
    }
}
