package eu.proexe.android.mpp.ui.news;

import javax.inject.Inject;

import eu.proexe.android.mpp.data.DataManager;
import eu.proexe.android.mpp.data.event.EventShowTab;
import eu.proexe.android.mpp.ui.base.BasePresenter;
import eu.proexe.android.mpp.util.RxEventBus;
import eu.proexe.android.mpp.util.RxUtil;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

/**
 * Created by Michał Trętowicz  on 18.03.17.
 */

public class NewsCategoriesPresenter extends BasePresenter<NewsCategoriesMvpView> {

    private Subscription showTabSub;

    @Inject
    public NewsCategoriesPresenter(DataManager mDataManager, RxEventBus mRxEventBus) {
        super(mDataManager, mRxEventBus);
    }

    @Override
    public void attachView(NewsCategoriesMvpView mvpView) {
        super.attachView(mvpView);

        RxUtil.unsubscribe(showTabSub);
        showTabSub = mRxEventBus.filteredObservable(EventShowTab.class)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<EventShowTab>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(EventShowTab eventShowTab) {
                        Timber.d("show tab" );

                        getMvpView().showTabWithName(eventShowTab.getCategory());
                    }
                });
    }

    @Override
    public void detachView() {
        super.detachView();
        RxUtil.unsubscribe(showTabSub);

    }
}
