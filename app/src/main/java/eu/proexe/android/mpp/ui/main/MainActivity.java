package eu.proexe.android.mpp.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.BottomBarTab;

import java.util.Arrays;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.proexe.android.mpp.R;
import eu.proexe.android.mpp.data.event.EventShowTab;
import eu.proexe.android.mpp.data.modals.PushNotificationsModal;
import eu.proexe.android.mpp.data.model.response.Article;
import eu.proexe.android.mpp.ui.base.BaseActivity;
import eu.proexe.android.mpp.ui.image.ImageListFragment;
import eu.proexe.android.mpp.ui.news.NewsCategoriesFragment;
import eu.proexe.android.mpp.ui.news.details.NewsDetailsActivity;
import eu.proexe.android.mpp.ui.search.SearchFragment;
import eu.proexe.android.mpp.ui.settings.SettingsFragment;
import eu.proexe.android.mpp.ui.videos.VideosListFragment;
import me.leolin.shortcutbadger.ShortcutBadger;
import timber.log.Timber;

import static eu.proexe.android.mpp.ui.news.details.NewsDetailsActivity.KEY_ARTICLE;
import static eu.proexe.android.mpp.ui.news.details.NewsDetailsActivity.KEY_NEWS_TYPE_ID;

public class MainActivity extends BaseActivity implements MainMvpView {

    private static final String TAG = MainActivity.class.getSimpleName();
    public static boolean closeAlbum = false;
    @Inject
    MainPresenter mMainPresenter;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.main_pager)
    ViewPager pager;
    @BindView(R.id.bottom_navigation)
    BottomBar bottomBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        BottomBarTab.newsDrawable = R.drawable.ic_news_v2;
        BottomBarTab.newsDrawableRed = R.drawable.ic_news_red;

        BottomBarTab.tvDrawable = R.drawable.ic_tv_v2;
        BottomBarTab.tvDrawableRed = R.drawable.ic_tv_red;

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        ShortcutBadger.removeCount(this); //for 1.1.4+

        if (getIntent()!=null&&getIntent().hasExtra("FROM_NOTIFICATION")){
            boolean FROM_NOTIFICATION = getIntent().getBooleanExtra("FROM_NOTIFICATION",false);
            if (FROM_NOTIFICATION){
                PushNotificationsModal bean = (PushNotificationsModal) getIntent().getSerializableExtra("PushNotificationsModal");
                Intent intent = new Intent(MainActivity.this, NewsDetailsActivity.class);

                Article article = new Article();
                article.setPKey(bean.getPkey());
                intent.putExtra(KEY_ARTICLE, article);
                intent.putExtra(KEY_NEWS_TYPE_ID, bean.getNewsTypeId());
                intent.putExtra("isFullImagePath", false);
                intent.putExtra("fullImagePath", "");
                startActivity(intent);
            }
        }

        pager.setAdapter(new PagesAdapter(getSupportFragmentManager(),
                Arrays.asList(new NewsCategoriesFragment(), SearchFragment.newInstance(),
                        VideosListFragment.newInstance(), ImageListFragment.newInstance(),
                        SettingsFragment.newInstance())));

        pager.setOffscreenPageLimit(8);

        mMainPresenter.attachView(this);




        pager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                try {
                    bottomBar.selectTabAtPosition(position);
                }catch (Throwable t) {
                    bottomBar.selectTabAtPosition(0);
                }
            }
        });


        bottomBar.setOnTabSelectListener(tabId -> {

                Timber.d("pager count " + tabId + " " + R.id.news);
                switch (tabId) {
                    case R.id.news:
                        closeAlbumDetail();

                        pager.setCurrentItem(0, false);
                        break;

                    case R.id.search:
                        closeAlbumDetail();

                        pager.setCurrentItem(1, false);
                        break;

                    case R.id.tv:
                        closeAlbumDetail();

                        pager.setCurrentItem(2, false);
                        break;

                    case R.id.image:
                        closeAlbumDetail();

                        pager.setCurrentItem(3, false);
                        break;

                    case R.id.settings:
                        closeAlbumDetail();
                        pager.setCurrentItem(4, false);
                        break;
                }
            });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Timber.d("on result");
        mMainPresenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mMainPresenter.detachView();
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {

        if (closeAlbumDetail()) return;

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.press_twice_to_exit, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(() -> doubleBackToExitPressedOnce=false, 2000);
    }

    private boolean closeAlbumDetail() {
        if (closeAlbum) {
            closeAlbum = false;
            mMainPresenter.closeAlbumDetail();
            return true;
        }
        return false;
    }


    @Override
    public void showTab(EventShowTab eventShowTab) {
        pager.setCurrentItem(0);
    }

    @Override
    public void setTextSize(float textSize) {

    }
}
