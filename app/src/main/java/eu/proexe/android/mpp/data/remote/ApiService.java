package eu.proexe.android.mpp.data.remote;

import eu.proexe.android.mpp.data.model.request.AddDeviceToken;
import eu.proexe.android.mpp.data.model.request.RequestEnvelope;
import eu.proexe.android.mpp.data.model.response.Response2;
import eu.proexe.android.mpp.data.model.response.SearchResponse;
import eu.proexe.android.mpp.data.model.response.album.AlbumDetailsResponse;
import eu.proexe.android.mpp.data.model.response.album.AlbumsResponse;
import eu.proexe.android.mpp.data.model.response.category.NewsCategoriesResponse;
import eu.proexe.android.mpp.data.model.response.category.NewsDetailsResponse;
import eu.proexe.android.mpp.data.model.response.video.VideosResponse;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import rx.Observable;

public interface ApiService {

    String ENDPOINT = "http://d2495236.u68.gohsphere.com/";
    String IMAGE_URL = "http://d2495236.u68.gohsphere.com/NewsImages/%s/";
    String IMAGE_URL_VIDEO = "http://www.awjonlinetv.com/Videos/Images/";


    @Headers({
            "Content-Type: application/soap+xml",
            "Accept-Charset: utf-8"
            })
    @POST("CategoryWiseNews.asmx")
    Observable<Response2> getArticles(@Body RequestEnvelope body);


    @Headers({
            "Content-Type: application/soap+xml",
            "Accept-Charset: utf-8"
            })
    @POST("GetAllVideos.asmx")
    Observable<VideosResponse> getVideos(@Body RequestEnvelope body);


    @Headers({
            "Content-Type: application/soap+xml",
            "Accept-Charset: utf-8"
            })
    @POST("ListAllNewsCategories.asmx")
    Observable<NewsCategoriesResponse> getAllCategories(@Body RequestEnvelope body);


    @Headers({
            "Content-Type: application/soap+xml",
            "Accept-Charset: utf-8"
            })
    @POST("GetNewsDescription.asmx")
    Observable<NewsDetailsResponse> getNewsDescription(@Body RequestEnvelope body);


    @Headers({
            "Content-Type: application/soap+xml",
            "Accept-Charset: utf-8"
            })
    @POST("PushNotifications.asmx")
    Observable<AddDeviceToken> addDeviceToken(@Body RequestEnvelope body);

    @Headers({
            "Content-Type: application/soap+xml",
            "Accept-Charset: utf-8"
            })
    @POST("GetImagesAlbums.asmx")
    Observable<AlbumsResponse> getImagesAlbums(@Body RequestEnvelope body);


    @Headers({
            "Content-Type: application/soap+xml",
            "Accept-Charset: utf-8"
            })
    @POST("GetImagesByAlbum.asmx")
    Observable<AlbumDetailsResponse> getImagesByAlbum(@Body RequestEnvelope body);



    @Headers({
            "Content-Type: application/soap+xml",
            "Accept-Charset: utf-8"
    })
    @POST("searchnews.asmx")
    Observable<SearchResponse> search(@Body RequestEnvelope body);

}
