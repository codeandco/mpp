package eu.proexe.android.mpp.ui.settings.text.size;

import javax.inject.Inject;

import eu.proexe.android.mpp.data.DataManager;
import eu.proexe.android.mpp.ui.base.BasePresenter;
import eu.proexe.android.mpp.util.RxEventBus;

/**
 * Created by Michał Trętowicz  on 12.03.17.
 */

public class TextSizeSettingsPresenter extends BasePresenter<TextSizeSettingsMvpView> {

    @Inject
    public TextSizeSettingsPresenter(DataManager mDataManager, RxEventBus mRxEventBus) {
        super(mDataManager, mRxEventBus);
    }

    @Override
    public void attachView(TextSizeSettingsMvpView mvpView) {
        super.attachView(mvpView);


        boolean useSystemSize = mDataManager.getPreferencesHelper().getUseSystemSize();
        getMvpView().showUseSystemSize(useSystemSize);

        int progress = mDataManager.getPreferencesHelper().getProgress();
        getMvpView().showProgress(progress);

        float textSize = mDataManager.getPreferencesHelper().getTextSize();
        getMvpView().showTextSize(textSize);



    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public void useSystemSizeChanged(boolean checked) {
        mDataManager.getPreferencesHelper().setUseSystemSize(checked);
    }

    public void textSizeChanged(float textSize) {
        mDataManager.getPreferencesHelper().setTextSize(textSize);
    }

    public void progressChanged(int progress) {
        mDataManager.getPreferencesHelper().setProgress(progress);
    }
}
