package eu.proexe.android.mpp.ui.settings.signin;

import eu.proexe.android.mpp.data.event.OnActivityResultEvent;
import eu.proexe.android.mpp.ui.base.MvpView;

/**
 * Created by Michał Trętowicz  on 12.03.17.
 */

public interface SignInMvpView extends MvpView {
    void checkTwitterLoginResult(OnActivityResultEvent onActivityResultEvent);
}
