package eu.proexe.android.mpp.ui.news.tabs;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

import eu.proexe.android.mpp.data.model.response.category.NewsCategory;

class PagesAdapter extends FragmentStatePagerAdapter {

    private final List<? extends Fragment> pages;
    private final List<NewsCategory> data;

    PagesAdapter(FragmentManager fm, List<NewsCategory> data, List<? extends Fragment> pages) {
        super(fm);
        this.data = data;
        this.pages = pages;
    }

    @Override
    public Fragment getItem(int position) {
        return pages.get(position);
    }

    @Override
    public int getCount() {
        return pages.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return data.get(position).NewsType;
    }
}