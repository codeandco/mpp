package eu.proexe.android.mpp.data.model.request;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

/**
 * Created by Michał Trętowicz  on 20.02.17.
 */
@Namespace(reference = "http://tempuri.org/")
@Root(name = "GetImagesbyAlbum")
public class GetImagesbyAlbum {

    @Element(name = "strPKey")
    private String strPKey;

    public String getStrPKey() {
        return strPKey;
    }

    public void setStrPKey(String strPKey) {
        this.strPKey = strPKey;
    }

}
