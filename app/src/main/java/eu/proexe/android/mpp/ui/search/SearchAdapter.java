package eu.proexe.android.mpp.ui.search;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jakewharton.rxrelay.PublishRelay;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.proexe.android.mpp.R;
import eu.proexe.android.mpp.data.ImageLoader;
import eu.proexe.android.mpp.data.model.response.Article;
import eu.proexe.android.mpp.injection.ActivityContext;
import rx.Observable;


public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {

    private final LayoutInflater layoutInflater;
    private final ImageLoader imageLoader;
    private List<Article> items = new ArrayList<>();
    private PublishRelay<Article> articlePublishRelay = PublishRelay.create();


    @Inject
    public SearchAdapter(@ActivityContext LayoutInflater layoutInflater, ImageLoader imageLoader) {
        this.layoutInflater = layoutInflater;
        this.imageLoader = imageLoader;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.item_news, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setData(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setItems(List<Article> items, String newsTypeIdExtra) {
        this.items = items;
        notifyDataSetChanged();
    }


    public Observable<Article> articleClicked() {
        return articlePublishRelay.asObservable();
    }


    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        public Article item;
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.text)
        TextView text;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        public void setData(Article item) {
            this.item = item;

            String url = item.getImageFullPath();

            imageLoader.load(image, url);
            text.setText(item.getHeader());

        }

        @Override
        public void onClick(View v) {
            articlePublishRelay.call(item);
        }
    }


}
                                