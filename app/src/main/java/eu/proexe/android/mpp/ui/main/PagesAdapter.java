package eu.proexe.android.mpp.ui.main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

class PagesAdapter extends FragmentStatePagerAdapter {

    final List<? extends Fragment> pages;

    public PagesAdapter(FragmentManager fm, List<? extends Fragment> pages) {
        super(fm);
        this.pages = pages;
    }

    @Override
    public Fragment getItem(int position) {
        return pages.get(position);
    }

    @Override
    public int getCount() {
        return pages.size();
    }
}