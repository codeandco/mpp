package eu.proexe.android.mpp.data;

import com.jakewharton.rxrelay.PublishRelay;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import eu.proexe.android.mpp.data.local.PreferencesHelper;
import eu.proexe.android.mpp.data.model.request.AddDeviceToken;
import eu.proexe.android.mpp.data.model.request.GetAlbums;
import eu.proexe.android.mpp.data.model.request.GetAllCategories;
import eu.proexe.android.mpp.data.model.request.GetCategoryWiseNews;
import eu.proexe.android.mpp.data.model.request.GetImagesbyAlbum;
import eu.proexe.android.mpp.data.model.request.GetNewsDescriptionByID;
import eu.proexe.android.mpp.data.model.request.GetSearchNews;
import eu.proexe.android.mpp.data.model.request.GetVideos;
import eu.proexe.android.mpp.data.model.request.RequestEnvelope;
import eu.proexe.android.mpp.data.model.request.Soap12Body;
import eu.proexe.android.mpp.data.model.response.Article;
import eu.proexe.android.mpp.data.model.response.Response2;
import eu.proexe.android.mpp.data.model.response.SearchResponse;
import eu.proexe.android.mpp.data.model.response.album.AlbumDetailItem;
import eu.proexe.android.mpp.data.model.response.album.AlbumDetailsResponse;
import eu.proexe.android.mpp.data.model.response.album.AlbumItem;
import eu.proexe.android.mpp.data.model.response.album.AlbumsResponse;
import eu.proexe.android.mpp.data.model.response.category.NewsCategoriesResponse;
import eu.proexe.android.mpp.data.model.response.category.NewsCategory;
import eu.proexe.android.mpp.data.model.response.category.NewsDetailsResponse;
import eu.proexe.android.mpp.data.model.response.video.Video;
import eu.proexe.android.mpp.data.model.response.video.VideosResponse;
import eu.proexe.android.mpp.data.remote.ApiService;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@Singleton
public class DataManager {

    private final ApiService mApiService;
    private final PreferencesHelper mPreferencesHelper;
    private final PublishRelay<CharSequence> searchProxy;

    @Inject
    public DataManager(ApiService apiService, PreferencesHelper preferencesHelper) {
        mApiService = apiService;
        mPreferencesHelper = preferencesHelper;
        searchProxy = PublishRelay.create();
    }

    public PublishRelay<CharSequence> getSearchProxy() {
        return searchProxy;
    }


    public PreferencesHelper getPreferencesHelper() {
        return mPreferencesHelper;
    }


    public Observable<List<Article>> getArticles(String newsTypeId) {

        RequestEnvelope envelope = new RequestEnvelope();
        Soap12Body body = new Soap12Body();
        GetCategoryWiseNews getCategoryWiseNews = new GetCategoryWiseNews();
        getCategoryWiseNews.setStrNewsTypeID(newsTypeId);
        getCategoryWiseNews.setXmlns("http://tempuri.org/");
        body.setGetCategoryWiseNews(getCategoryWiseNews);
        envelope.setBody(body);

        return mApiService.getArticles(envelope).map(Response2::getArticle);
    }

    public Observable<List<Video>> getVideos() {
        RequestEnvelope envelope = new RequestEnvelope();
        Soap12Body body = new Soap12Body();
        body.setGetVideos(new GetVideos());
        envelope.setBody(body);

        return mApiService.getVideos(envelope).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(VideosResponse::getVideos);
    }


    public Observable<List<AlbumDetailItem>> getImagesByAlbum(String pKey) {
        RequestEnvelope envelope = new RequestEnvelope();
        Soap12Body body = new Soap12Body();
        GetImagesbyAlbum getImagesbyAlbum = new GetImagesbyAlbum();
        getImagesbyAlbum.setStrPKey(pKey);
        body.setGetImagesbyAlbum(getImagesbyAlbum);
        envelope.setBody(body);

        return mApiService.getImagesByAlbum(envelope).subscribeOn(Schedulers.io())
                .map(AlbumDetailsResponse::getAlbumItems)
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<NewsCategory>> getAllCategories() {
        RequestEnvelope envelope = new RequestEnvelope();
        Soap12Body body = new Soap12Body();
        body.setGetAllCategories(new GetAllCategories());
        envelope.setBody(body);

        return mApiService.getAllCategories(envelope)
                .map(NewsCategoriesResponse::getCategories).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<AlbumItem>> getAlbums() {

        RequestEnvelope envelope = new RequestEnvelope();
        Soap12Body body = new Soap12Body();
        body.setGetAlbums(new GetAlbums());
        envelope.setBody(body);

        return mApiService.getImagesAlbums(envelope).subscribeOn(Schedulers.io())
                .map(AlbumsResponse::getAlbumItems)
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<NewsDetailsResponse> getNewsDescription(String pKey) {
        System.out.println("--- > pKey : " + pKey);


        RequestEnvelope envelope = new RequestEnvelope();
        Soap12Body body = new Soap12Body();
        GetNewsDescriptionByID getNewsDescriptionByID = new GetNewsDescriptionByID();
        getNewsDescriptionByID.setStrPKey(pKey);
        body.setGetNewsDescriptionByID(getNewsDescriptionByID);
        envelope.setBody(body);
        System.out.println("--- > getNewsDescriptionByID : " + getNewsDescriptionByID.getStrPKey());
        System.out.println("--- > body : " + body.getGetNewsDescriptionByID());
        System.out.println("--- > envelope : " + envelope.toString());
        return mApiService.getNewsDescription(envelope).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<AddDeviceToken> addDeviceToken(String deviceToken) {


        RequestEnvelope envelope = new RequestEnvelope();
        Soap12Body body = new Soap12Body();
        AddDeviceToken mAddDeviceToken = new AddDeviceToken();
        mAddDeviceToken.setDeviceToken(deviceToken);
        body.setmAddDeviceToken(mAddDeviceToken);
        envelope.setBody(body);
        System.out.println("--- > getDeviceToken : " + mAddDeviceToken.getDeviceToken());
        System.out.println("--- > body : " + body.getmAddDeviceToken());
        System.out.println("--- > envelope : " + envelope.toString());
        return mApiService.addDeviceToken(envelope).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }


    public Observable<SearchResponse> search(String search) {

        RequestEnvelope envelope = new RequestEnvelope();
        Soap12Body body = new Soap12Body();
        GetSearchNews getSearchNews = new GetSearchNews();
        getSearchNews.setStrText(search);
        body.setGetSearchNews(getSearchNews);
        envelope.setBody(body);
        return mApiService.search(envelope).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
