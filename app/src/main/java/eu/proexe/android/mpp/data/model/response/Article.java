package eu.proexe.android.mpp.data.model.response;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

@Root(name = "AllInitiatives")
public class Article implements Serializable {
    @Element(name = "SortOrder", required = false)
    private String sortOrder;

    @Element(name = "NewsSDesc", required = false)
    private String newsSDesc;

    @Element(name = "Display", required = false)
    private String display;

    @Element(name = "ThumbNailName", required = false)
    private String thumbNailName;

    @Element(name = "ImageTitle", required = false)
    private String imageTitle;

    @Element(name = "CreatedUser", required = false)
    private String createdUser;

    @Element(name = "PKey", required = false)
    private String pKey;

    @Element(name = "ExpiryDate", required = false)
    private String expiryDate;

    @Element(name = "NewsDesc", required = false)
    private String newsDesc;

    @Element(name = "NewsTypeID", required = false)
    private String newsTypeID;

    @Element(name = "ImageName", required = false)
    private String imageName;

    @Element(name = "ThumbNailTitle", required = false)
    private String thumbNailTitle;

    @Element(name = "StartDate", required = false)
    private String startDate;

    @Element(name = "Header", required = false)
    private String header;

    @Element(name = "NewsType", required = false)
    private String newsType;

    @Element(name = "NewsletterPkey", required = false)
    private String newsletterPkey;

    @Element(name = "CreatedDate", required = false)
    private String createdDate;

    @Element(name = "IsLatest", required = false)
    private String isLatest;


    @Element(name = "ImageFullPath", required = false)
    private String imageFullPath;

    public void setImageFullPath(String imageFullPath) {
        this.imageFullPath = imageFullPath;
    }

    public String getImageFullPath() {
        return imageFullPath;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getNewsSDesc() {
        return newsSDesc;
    }

    public void setNewsSDesc(String newsSDesc) {
        this.newsSDesc = newsSDesc;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getThumbNailName() {
        return thumbNailName;
    }

    public void setThumbNailName(String thumbNailName) {
        this.thumbNailName = thumbNailName;
    }

    public String getImageTitle() {
        return imageTitle;
    }

    public void setImageTitle(String imageTitle) {
        this.imageTitle = imageTitle;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getPKey() {
        return pKey;
    }

    public void setPKey(String pKey) {
        this.pKey = pKey;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getNewsDesc() {
        return newsDesc;
    }

    public void setNewsDesc(String newsDesc) {
        this.newsDesc = newsDesc;
    }

    public String getNewsTypeID() {
        return newsTypeID;
    }

    public void setNewsTypeID(String newsTypeID) {
        this.newsTypeID = newsTypeID;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getThumbNailTitle() {
        return thumbNailTitle;
    }

    public void setThumbNailTitle(String thumbNailTitle) {
        this.thumbNailTitle = thumbNailTitle;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getNewsType() {
        return newsType;
    }

    public void setNewsType(String newsType) {
        this.newsType = newsType;
    }

    public String getNewsletterPkey() {
        return newsletterPkey;
    }

    public void setNewsletterPkey(String newsletterPkey) {
        this.newsletterPkey = newsletterPkey;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getIsLatest() {
        return isLatest;
    }

    public void setIsLatest(String isLatest) {
        this.isLatest = isLatest;
    }
}
