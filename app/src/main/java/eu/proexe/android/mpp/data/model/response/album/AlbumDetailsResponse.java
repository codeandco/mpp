package eu.proexe.android.mpp.data.model.response.album;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Michał Trętowicz  on 09.02.17.
 */

@Root(name = "soap:Envelope")
public class AlbumDetailsResponse {

    @Path("Body/GetImagesbyAlbumResponse/GetImagesbyAlbumResult/diffgram/NewDataSet")
    @ElementList(inline = true)
    private List<AlbumDetailItem> albumItems;

    public List<AlbumDetailItem> getAlbumItems() {
        return albumItems;
    }
}
