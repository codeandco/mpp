package eu.proexe.android.mpp.ui.image;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import eu.proexe.android.mpp.R;
import eu.proexe.android.mpp.data.event.EventTabs;
import eu.proexe.android.mpp.data.model.response.album.AlbumItem;
import eu.proexe.android.mpp.data.model.response.category.NewsCategory;
import eu.proexe.android.mpp.ui.base.BaseActivity;
import eu.proexe.android.mpp.ui.image.detail.AlbumDetailFragment;
import eu.proexe.android.mpp.util.VerticalSpaceItemDecoration;
import eu.proexe.android.mpp.util.ViewUtil;
import rx.Observable;
import timber.log.Timber;

/**
 * Created by Michał Trętowicz  on 09.02.17.
 */

public class ImageListFragment extends Fragment implements ImageListMvpView, OnBackPressedFragment {

    @Inject
    ImageListPresenter presenter;
    @BindView(R.id.list)
    RecyclerView list;
    @BindView(R.id.progress)
    View progress;
    @Inject
    ImagesAdapter adapter;
    @BindView(R.id.tabs)
    TabLayout tabLayout;
    private Unbinder bind;

    public static ImageListFragment newInstance() {
        ImageListFragment fragment = new ImageListFragment();
        fragment.setArguments(new Bundle());
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflated = inflater.inflate(R.layout.fragment_images_list, container, false);
        bind = ButterKnife.bind(this, inflated);

        NewsCategory tag = new NewsCategory();
        tag.NewsType = "Images";
        tabLayout.addTab(tabLayout.newTab().setTag(tag).setText("Photos"));
        return inflated;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        tabLayout.setSmoothScrollingEnabled(true);
        list.addItemDecoration(new VerticalSpaceItemDecoration(ViewUtil.dpToPx(2)));
        list.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        list.setAdapter(adapter);

        styleTab(12);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                NewsCategory newsCategory = (NewsCategory) tab.getTag();

                if (!newsCategory.NewsType.equals("Photos")) {
                    presenter.showTab(newsCategory);
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        presenter.attachView(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        tabLayout.post(() -> {
            tabLayout.getTabAt(tabLayout.getTabCount() - 1).select();
            tabLayout.setScrollPosition(tabLayout.getTabCount() - 1, 0f, true);
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        try {
            tabLayout.post(() -> {
                tabLayout.getTabAt(tabLayout.getTabCount() - 1).select();
                tabLayout.setScrollPosition(tabLayout.getTabCount() - 1, 0f, true);
            });

        } catch (Throwable t) {

        }
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((BaseActivity) getActivity()).activityComponent().inject(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        bind.unbind();
        presenter.detachView();
    }

    @Override
    public void showLoading(boolean show) {
        progress.animate().alpha(show ? 1 : 0).setDuration(250);
    }

    @Override
    public void showErrorLoading() {

    }

    @Override
    public void showItems(List<AlbumItem> data) {
        adapter.setItems(data);
    }

    @Override
    public void showEmpty() {
    }

    @Override
    public void hideEmpty() {
    }

    @Override
    public Observable<AlbumItem> itemClicked() {
        return adapter.getClickedRelay();
    }

    @Override
    public void showDetails(AlbumItem albumItem) {
        getChildFragmentManager().beginTransaction().replace(R.id.details,
                AlbumDetailFragment.newInstance(albumItem.getPKey() + "", albumItem.year, albumItem.albumName))
                .addToBackStack(null).commitAllowingStateLoss();
    }

    @Override
    public void closeAlbumDetail() {
        try {
            getChildFragmentManager().popBackStackImmediate();
        } catch (IllegalStateException ex) {
            Timber.e(ex, "error in closeAlbumDetail");
        }
    }

    @Override
    public void showAdditionalTabs(EventTabs eventTabs) {
//
//        for (NewsCategory name : eventTabs.getTabsNames()) {
//            tabLayout.addTab(tabLayout.newTab().setTag(name).setText(name.NewsType), 0);
//        }
//
//        tabLayout.post(() -> {
//            tabLayout.getTabAt(tabLayout.getTabCount() - 1).select();
//
//            tabLayout.setScrollPosition(tabLayout.getTabCount() - 1, 0f, true);
//        });

    }

    @Override
    public boolean onBackPressed() {
        return getChildFragmentManager().popBackStackImmediate();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setTextSize(float textSize) {
        adapter.setTextSize(textSize);

        styleTab(textSize);
    }


    void styleTab(float size) {
        TabLayout.Tab tab = tabLayout.getTabAt(0);
        if (tab != null) {

            if(tab.getCustomView() != null) {
                TextView tabTextView = (TextView) tab.getCustomView();
                tabTextView.setTextColor(Color.WHITE);

                tabTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
                tabTextView.setGravity(Gravity.CENTER);
                tabTextView.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
                tabTextView.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;

                tabTextView.setText(tab.getText());
                tabTextView.setTypeface(null, Typeface.BOLD);
                return;
            }

            TextView tabTextView = new TextView(getContext());

            tabTextView.setTextColor(Color.WHITE);

            tabTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
            tabTextView.setGravity(Gravity.CENTER);
            tab.setCustomView(tabTextView);


            tabTextView.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
            tabTextView.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;

            tabTextView.setText(tab.getText());
            tabTextView.setTypeface(null, Typeface.BOLD);
        }
    }
}
