package eu.proexe.android.mpp.data.model.response.category;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by Michał Trętowicz  on 09.02.17.
 */
@Root(name = "AllInitiatives")
public class NewsCategory {

    @Element(name = "NewsTypeID")
    public String NewsTypeID;

    @Element(name = "NewsType")
    public String NewsType;

    public NewsCategory() {
    }

    public void setNewsType(String newsType) {
        NewsType = newsType;
    }

    public NewsCategory(String newsType, String newsTypeID) {
        NewsTypeID = newsTypeID;
        NewsType = newsType;
    }

    @Override
    public String toString() {
        return "NewsCategory{" +
                "NewsTypeID='" + NewsTypeID + '\'' +
                ", NewsType='" + NewsType + '\'' +
                '}';
    }
}
