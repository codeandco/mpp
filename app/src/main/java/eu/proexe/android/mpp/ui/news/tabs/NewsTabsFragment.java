package eu.proexe.android.mpp.ui.news.tabs;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import eu.proexe.android.mpp.R;
import eu.proexe.android.mpp.data.model.response.category.NewsCategory;
import eu.proexe.android.mpp.ui.base.BaseActivity;
import eu.proexe.android.mpp.ui.news.list.NewsListFragment;
import eu.proexe.android.mpp.util.StylingUtils;

public class NewsTabsFragment extends Fragment implements NewsTabsMvpView {

    @Inject
    NewsTabsPresenter presenter;

    @BindView(R.id.pager)
    ViewPager pager;

    @BindView(R.id.tabs)
    TabLayout tabLayout;

    private Unbinder bind;
    private TabLayout.OnTabSelectedListener listener;

    public static NewsTabsFragment newInstance(List<String> allowedCategories)  {
        NewsTabsFragment fragment = new NewsTabsFragment();
        Bundle args = new Bundle();
        args.putStringArrayList("extra", new ArrayList<>(allowedCategories));
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflated = inflater.inflate(R.layout.fragment_news, container, false);
        bind = ButterKnife.bind(this, inflated);


        pager.setOffscreenPageLimit(6);

        if(getArguments().getStringArrayList("extra").size() < 4) {
            tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
            tabLayout.setTabMode(TabLayout.MODE_FIXED);
        }

        //tabLayout.setTabTextColors(Color.BLACK,Color.BLACK);

        return inflated;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.attachView(this);
        presenter.load(getArguments().getStringArrayList("extra"));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((BaseActivity) getActivity()).activityComponent().inject(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(listener != null) {
            tabLayout.removeOnTabSelectedListener(listener);
        }
        presenter.detachView();
        bind.unbind();
    }

    @Override
    public void showLoading(boolean show) {
//        progress.animate().alpha(show ? 1 : 0).setDuration(250);
    }

    @Override
    public void showErrorLoading() {
    }

    @BindColor(R.color.inactive_tab_text)
    int inactiveTabText;

    @Override
    public void showItems(List<NewsCategory> data) {

        List<NewsListFragment> fragments = new ArrayList<>();


        for (NewsCategory newsCategory : data) {
            fragments.add(NewsListFragment.newInstance(newsCategory));
        }

        presenter.showCategories(new ArrayList<>(data));

        pager.setAdapter(new PagesAdapter(getChildFragmentManager(), data, fragments));

        tabLayout.setupWithViewPager(pager);


        listener = StylingUtils.styleTabs(pager, tabLayout,false, 12, new int[]{inactiveTabText, Color.BLACK});
        tabLayout.addOnTabSelectedListener(listener);

        try {
            presenter.refreshTextSize();
        }catch (Throwable ignored) {

        }
    }

    @Override
    public void showEmpty() {
    }

    @Override
    public void hideEmpty() {
    }

    @Override
    public void showTabWithName(NewsCategory category) {

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tabAt = tabLayout.getTabAt(i);
            if (tabAt.getText().toString().equals(category.NewsType)) {
                int finalI = i;
                tabLayout.post(() -> {
                    tabLayout.getTabAt(finalI).select();
                    tabLayout.setScrollPosition(finalI, 0f, true);
                });
            }
        }
    }

    @Override
    public void setTextSize(float textSize) {
        if(listener != null) {
            tabLayout.removeOnTabSelectedListener(listener);
        }
        listener = StylingUtils.restyle(pager, tabLayout,false,textSize, new int[]{inactiveTabText, Color.BLACK});
        tabLayout.addOnTabSelectedListener(listener);
    }
}
