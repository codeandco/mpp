package eu.proexe.android.mpp.util;

import android.graphics.Color;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Michał Trętowicz  on 17.03.17.
 */

public class StylingUtils {


    public static TabLayout.OnTabSelectedListener restyle(ViewPager pager, TabLayout tabs,boolean color, float textsize, int colors[]) {
        for (int i = 0; i < tabs.getTabCount(); i++) {

            TabLayout.Tab tab = tabs.getTabAt(i);
            if (tab != null && tab.getCustomView() != null) {

                TextView tabTextView = (TextView) tab.getCustomView();

                if(color) {
                    tabTextView.setTextColor(Color.WHITE);
                }
                tabTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textsize);
                tabTextView.setGravity(Gravity.CENTER);

                tabTextView.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
                tabTextView.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;

                tabTextView.setText(tab.getText());

                // First tab is the selected tab, so if i==0 then set BOLD typeface
                if (i == 0) {
                    tabTextView.setTypeface(null, Typeface.BOLD);

                    if(colors != null) {
                        tabTextView.setTextColor(colors[1]);
                    }
                }

            }

        }

        TabLayout.OnTabSelectedListener listener = new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition());

                TextView text = (TextView) tab.getCustomView();
                if(text != null) {
                    text.setTypeface(null, Typeface.BOLD);

                    if (colors != null) {
                        text.setTextColor(colors[1]);
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                TextView text = (TextView) tab.getCustomView();
                if(text != null) {
                    text.setTypeface(null, Typeface.NORMAL);
                    if (colors != null) {
                        text.setTextColor(colors[0]);
                    }
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }

        };
        tabs.addOnTabSelectedListener(listener);
        return listener;
    }


    public static TabLayout.OnTabSelectedListener styleTabs(ViewPager pager, TabLayout tabs,boolean color, int textsize, int colors[]) {
        for (int i = 0; i < tabs.getTabCount(); i++) {

            TabLayout.Tab tab = tabs.getTabAt(i);
            if (tab != null) {

                TextView tabTextView = new TextView(tabs.getContext());

                if(color) {
                    tabTextView.setTextColor(Color.WHITE);
                }
                tabTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textsize);
                tabTextView.setGravity(Gravity.CENTER);
                tab.setCustomView(tabTextView);


                tabTextView.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
                tabTextView.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;

                tabTextView.setText(tab.getText());



                // First tab is the selected tab, so if i==0 then set BOLD typeface
                if (i == 0) {
                    tabTextView.setTypeface(null, Typeface.BOLD);

                    if(colors != null) {
                        tabTextView.setTextColor(colors[1]);
                    }
                }

            }

        }

        TabLayout.OnTabSelectedListener listener = new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition());

                TextView text = (TextView) tab.getCustomView();
                text.setTypeface(null, Typeface.BOLD);

                if(colors != null) {
                    text.setTextColor(colors[1]);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                TextView text = (TextView) tab.getCustomView();
                text.setTypeface(null, Typeface.NORMAL);
                if(colors != null) {
                    text.setTextColor(colors[0]);
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }

        };
        tabs.addOnTabSelectedListener(listener);
        return listener;
    }
}
