package eu.proexe.android.mpp.data.event;

/**
 * Created by Michał Trętowicz  on 19.03.17.
 */

public class ChangeTextSize {

    private float textSize;

    public float getTextSize() {
        return textSize;
    }

    public ChangeTextSize(float textSize) {
        this.textSize = textSize;
    }
}
