package eu.proexe.android.mpp.util;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import timber.log.Timber;

/**
 * Created by Michał Trętowicz  on 12.02.17.
 */

public class IntentUtils {

    public static void openWebsite(Context context, String url) {

        if (!url.startsWith("https://") && !url.startsWith("http://")) {
            url = "http://" + url;
        }
        Intent openUrlIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));

        try {
            context.startActivity(openUrlIntent);
        } catch (ActivityNotFoundException ex) {
            Timber.e(ex, "open website error");
        }

    }
}
