package eu.proexe.android.mpp.ui.image;

import javax.inject.Inject;

import eu.proexe.android.mpp.data.DataManager;
import eu.proexe.android.mpp.data.EventCloseAlbumDetail;
import eu.proexe.android.mpp.data.event.EventShowTab;
import eu.proexe.android.mpp.data.event.EventTabs;
import eu.proexe.android.mpp.data.model.response.album.AlbumItem;
import eu.proexe.android.mpp.data.model.response.category.NewsCategory;
import eu.proexe.android.mpp.ui.base.LoadingPresenter;
import eu.proexe.android.mpp.util.RxEventBus;
import eu.proexe.android.mpp.util.RxUtil;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Michał Trętowicz  on 09.02.17.
 */

public class ImageListPresenter extends LoadingPresenter<ImageListMvpView> {

    private Subscription subscription;
    private Subscription itemClickSubscription;
    private Subscription subscribe;
    private Subscription tabsSub;

    @Inject
    public ImageListPresenter(DataManager mDataManager, RxEventBus mRxEventBus) {
        super(mDataManager, mRxEventBus);
    }

    @Override
    public void attachView(ImageListMvpView mvpView) {
        super.attachView(mvpView);

        RxUtil.unsubscribe(subscription);
        subscription = mDataManager.getAlbums()
                .subscribe(new LoadingSubscriber<>());


        getMvpView().setTextSize(mDataManager.getPreferencesHelper().getTextSize());

        RxUtil.unsubscribe(itemClickSubscription);
        itemClickSubscription = getMvpView().itemClicked()
                .subscribe(new Subscriber<AlbumItem>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(AlbumItem albumItem) {
                        getMvpView().showDetails(albumItem);
                    }
                });

        RxUtil.unsubscribe(subscribe);

        subscribe = mRxEventBus.filteredObservable(EventCloseAlbumDetail.class)
                .subscribe(new Subscriber<EventCloseAlbumDetail>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(EventCloseAlbumDetail eventCloseAlbumDetail) {
                        getMvpView().closeAlbumDetail();
                    }
                });


        RxUtil.unsubscribe(tabsSub);
        tabsSub = mRxEventBus.filteredObservable(EventTabs.class)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<EventTabs>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(EventTabs eventTabs) {
                        getMvpView().showAdditionalTabs(eventTabs);
                    }
                });
    }

    @Override
    public void detachView() {
        super.detachView();
        RxUtil.unsubscribe(subscription);
        RxUtil.unsubscribe(itemClickSubscription);
        RxUtil.unsubscribe(subscribe);
        RxUtil.unsubscribe(tabsSub);

    }

    public void showTab(NewsCategory name) {
        mRxEventBus.post(new EventShowTab(name));
    }
}
