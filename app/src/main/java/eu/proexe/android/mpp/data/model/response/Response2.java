package eu.proexe.android.mpp.data.model.response;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "soap:Envelope")
@NamespaceList({
        @Namespace(prefix = "soap", reference = "http://www.w3.org/2003/05/soap-envelope")
               })
public class Response2 {

    @Path("Body/GetCategoryWiseNewsResponse/GetCategoryWiseNewsResult/diffgram/NewDataSet")
    @ElementList(inline = true)
    private List<Article> article;

    public List<Article> getArticle() {
        return article;
    }

    public void setArticle(List<Article> article) {
        this.article = article;
    }
}
