package eu.proexe.android.mpp.ui.settings.notification;

import javax.inject.Inject;

import eu.proexe.android.mpp.data.DataManager;
import eu.proexe.android.mpp.ui.base.BasePresenter;
import eu.proexe.android.mpp.util.RxEventBus;

/**
 * Created by Michał Trętowicz  on 12.03.17.
 */

public class NotificationSettingsPresenter extends BasePresenter<NotificationSettingsMvpView> {

    @Inject
    public NotificationSettingsPresenter(DataManager mDataManager, RxEventBus mRxEventBus) {
        super(mDataManager, mRxEventBus);
    }

    @Override
    public void attachView(NotificationSettingsMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }


}
