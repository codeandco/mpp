package eu.proexe.android.mpp.ui.main;

import eu.proexe.android.mpp.data.event.EventShowTab;
import eu.proexe.android.mpp.ui.base.MvpView;

public interface MainMvpView extends MvpView {
    void showTab(EventShowTab eventShowTab);
}
