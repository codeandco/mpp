package eu.proexe.android.mpp.util;

import java.util.Locale;

public class NumbersUtils {

    public static String formatWithThousandComma(int number) {
        return String.format(Locale.US, "%,d", number);
    }

    public static String format(int number) {
        return String.format(Locale.US, "%d", number);
    }
}
