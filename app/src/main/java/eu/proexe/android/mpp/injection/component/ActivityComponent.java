package eu.proexe.android.mpp.injection.component;

import dagger.Subcomponent;
import eu.proexe.android.mpp.injection.PerActivity;
import eu.proexe.android.mpp.injection.module.ActivityModule;
import eu.proexe.android.mpp.ui.image.ImageListFragment;
import eu.proexe.android.mpp.ui.image.detail.AlbumDetailFragment;
import eu.proexe.android.mpp.ui.main.MainActivity;
import eu.proexe.android.mpp.ui.news.NewsCategoriesFragment;
import eu.proexe.android.mpp.ui.news.details.NewsDetailsActivity;
import eu.proexe.android.mpp.ui.news.list.NewsListFragment;
import eu.proexe.android.mpp.ui.news.tabs.NewsTabsFragment;
import eu.proexe.android.mpp.ui.search.SearchFragment;
import eu.proexe.android.mpp.ui.settings.SettingsFragment;
import eu.proexe.android.mpp.ui.settings.about.AboutFragment;
import eu.proexe.android.mpp.ui.settings.notification.NotificationSettingsMvpView;
import eu.proexe.android.mpp.ui.settings.notifications_2.NotiFragment;
import eu.proexe.android.mpp.ui.settings.privacy.PrivacyPolicyFragment;
import eu.proexe.android.mpp.ui.settings.signin.SignInFragment;
import eu.proexe.android.mpp.ui.settings.terms.TermsFragment;
import eu.proexe.android.mpp.ui.settings.text.size.TextSizeSettingsFragment;
import eu.proexe.android.mpp.ui.splash.SplashActivity;
import eu.proexe.android.mpp.ui.videos.VideosListFragment;
import eu.proexe.android.mpp.ui.widgets.TextSizeAwareTextView;

/**
 * This component inject dependencies to all Activities across the application
 */
@PerActivity
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(SplashActivity splashActivity);

    void inject(MainActivity mainActivity);

    void inject(NewsTabsFragment newsTabsFragment);

    void inject(NewsListFragment newsListFragment);

    void inject(VideosListFragment videosListFragment);

    void inject(SearchFragment searchFragment);

    void inject(ImageListFragment imageFragment);

    void inject(SettingsFragment settingsFragment);

    void inject(NewsDetailsActivity newsDetailsActivity);

    void inject(AlbumDetailFragment albumDetailFragment);

    void inject(NotificationSettingsMvpView notificationSettingsMvpView);

    void inject(TextSizeSettingsFragment textSizeSettingsFragment);

    void inject(SignInFragment signInFragment);

    void inject(AboutFragment aboutFragment);

    void inject(PrivacyPolicyFragment privacyPolicyFragment);

    void inject(TermsFragment termsFragment);

    void inject(TextSizeAwareTextView textSizeAwareTextView);

    void inject(NewsCategoriesFragment newsCategoriesFragment);

    void inject(NotiFragment notiFragment);

}
