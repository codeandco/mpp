package eu.proexe.android.mpp.ui.news.details;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.PlusShare;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import org.joda.time.DateTime;

import java.net.MalformedURLException;
import java.net.URL;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import eu.proexe.android.mpp.R;
import eu.proexe.android.mpp.data.ImageLoader;
import eu.proexe.android.mpp.data.model.response.Article;
import eu.proexe.android.mpp.data.model.response.category.NewsDetails;
import eu.proexe.android.mpp.data.remote.ApiService;
import eu.proexe.android.mpp.ui.base.BaseActivity;
import eu.proexe.android.mpp.ui.widgets.TextSizeAwareTextView;
import eu.proexe.android.mpp.util.TimeSinceUtil;
import eu.proexe.android.mpp.util.ViewUtil;
import timber.log.Timber;

@SuppressWarnings("ConstantConditions")
public class NewsDetailsActivity extends BaseActivity implements NewsDetailsMvpView, GoogleApiClient.OnConnectionFailedListener {

    public static final String KEY_ARTICLE = "ARTICLE";
    public static final String KEY_NEWS_TYPE_ID = "NEWS_TYPE_ID";

    @Inject
    NewsDetailsPresenter presenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.image)
    ImageView image;

    @BindView(R.id.content)
    TextSizeAwareTextView content;

    @BindView(R.id.time)
    TextView time;

    @BindView(R.id.app_bar)
    AppBarLayout appBarLayout;

    @BindView(R.id.root)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.nested_scroll)
    NestedScrollView nestedScrollView;

    @BindView(R.id.go_up)
    ImageView goUpArrow;

    @BindView(R.id.progress)
    View progress;

    @Inject
    ImageLoader imageLoader;

    private GoogleApiClient mGoogleApiClient;


    public static void start(Context context,
                             Article article,
                             String newsTypeId,
                             String fullImagePath,
                             boolean isFullImagePath) {
        Intent intent = new Intent(context, NewsDetailsActivity.class);
        intent.putExtra(KEY_ARTICLE, article);
        intent.putExtra(KEY_NEWS_TYPE_ID, newsTypeId);
        intent.putExtra("isFullImagePath", isFullImagePath);
        intent.putExtra("fullImagePath", fullImagePath);
        context.startActivity(intent);
    }

    boolean isFullImagePath() {
        return getIntent().getBooleanExtra("isFullImagePath", false);
    }


    String getFullImagePath() {
        return getIntent().getStringExtra("fullImagePath");
    }

    private Article getArticle() {
        return (Article) getIntent().getSerializableExtra(KEY_ARTICLE);
    }

    private String getNewsTypeId() {
        return getIntent().getStringExtra(KEY_NEWS_TYPE_ID);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);

        setContentView(R.layout.activity_scrolling);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);


        GoogleSignInOptions gso =
                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 1, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        getSupportActionBar().setTitle(" ");
        toolbar.setNavigationOnClickListener(v -> finish());

        presenter.attachView(this);

        presenter.loadDetails(getArticle().getPKey());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);

//        AccessToken accessToken = AccessToken.getCurrentAccessToken();
//        if (accessToken == null || accessToken.isExpired()) {
//            menu.getItem(0).getSubMenu().removeItem(R.id.fb);
//        }
//
//        TwitterSession activeSession = Twitter.getSessionManager().getActiveSession();
//
//        if (activeSession == null) {
//            menu.getItem(0).getSubMenu().removeItem(R.id.tw);
//        }
//
//
//        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
//        if (opr.isDone()) {
//
//            GoogleSignInResult result = opr.get();
//
//            if (result == null || !result.isSuccess() || result.getSignInAccount() == null) {
//                menu.getItem(0).getSubMenu().removeItem(R.id.gplus);
//            }
//
//
//            if (menu.getItem(0).getSubMenu().size() == 0) {
//                menu.clear();
//            }
//
//        } else {
//
//            opr.setResultCallback(result -> {
//                if (result == null || !result.isSuccess() || result.getSignInAccount() == null) {
//                    menu.getItem(0).getSubMenu().removeItem(R.id.gplus);
//                }
//
//                if (menu.getItem(0).getSubMenu().size() == 0) {
//                    menu.clear();
//                }
//
//            });
//        }


        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        String shareLink = "http://www.mpp-me.com/";
        switch (item.getItemId()) {
            case R.id.fb:

                ShareLinkContent content = new ShareLinkContent.Builder()
                        .setContentUrl(Uri.parse(shareLink))
                        .build();

                ShareDialog shareDialog = new ShareDialog(this);
                shareDialog.show(content, ShareDialog.Mode.AUTOMATIC);


                return true;

            case R.id.tw:

                try {
                    TweetComposer.Builder builder = new TweetComposer.Builder(this)
                            .url(new URL(Uri.parse(shareLink).toString()));
                    builder.show();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                return true;

            case R.id.gplus:


                Intent shareIntent = new PlusShare.Builder(this)
                        .setType("text/plain")
                        .setText(getArticle().getHeader())
                        .setContentUrl(Uri.parse(shareLink))
                        .getIntent();

                startActivityForResult(shareIntent, 0);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void showLoading(boolean show) {
        progress.animate().alpha(show ? 1 : 0).setDuration(250);
    }

    @Override
    public void showErrorLoading() {
        finish();
    }

    @Override
    public void showItems(NewsDetails data) {
        String url =
                isFullImagePath() ? getFullImagePath() :

                String.format(ApiService.IMAGE_URL, getNewsTypeId()) + data.ImageName;

        System.out.println("Image url Details screen :  " + url);
        imageLoader.load(image, url);


        content.setText(ViewUtil.fromHtml(data.NewsDesc));


        content.post(() -> goUpArrow.post(this::showOrHideArrow));

        if (data.StartDate != null) {
            long time = DateTime.parse(data.StartDate).toDate().getTime();
            this.time.setText(TimeSinceUtil.getTimeAgo(time, this));
        }
    }

    private void showOrHideArrow() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int height = size.y;

        if (goUpArrow.getY() < height) {
            goUpArrow.setVisibility(View.INVISIBLE);
        } else {
            goUpArrow.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showEmpty() {
    }

    @Override
    public void hideEmpty() {
    }

    @OnClick(R.id.go_up)
    void goUp() {
        nestedScrollView.smoothScrollTo(0, 0);
        appBarLayout.setExpanded(true, true);
    }

    @OnClick(R.id.zoom_in)
    void zoomIn() {
        content.decreaseTextSize();
        Timber.d("zoom in");

        content.post(this::showOrHideArrow);
    }

    @OnClick(R.id.zoom_out)
    void zoomOut() {
        content.increaseTextSize();
        content.post(this::showOrHideArrow);
    }

    @Override
    public void showTextSize(float textSize) {
        content.progress = textSize;
        content.setTextSize(textSize);
        time.setTextSize(textSize);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void setTextSize(float textSize) {

    }
}
