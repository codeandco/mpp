package eu.proexe.android.mpp.ui.search;

import com.jakewharton.rxrelay.PublishRelay;

import javax.inject.Inject;

import eu.proexe.android.mpp.data.DataManager;
import eu.proexe.android.mpp.data.model.response.Article;
import eu.proexe.android.mpp.data.model.response.SearchResponse;
import eu.proexe.android.mpp.ui.base.LoadingPresenter;
import eu.proexe.android.mpp.util.RxEventBus;
import eu.proexe.android.mpp.util.RxUtil;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Michał Trętowicz  on 18.03.17.
 */

public class SearchPresenter extends LoadingPresenter<SearchMvpView> {

    private Subscription searchSubscription;

    @Inject
    public SearchPresenter(DataManager mDataManager, RxEventBus mRxEventBus) {
        super(mDataManager, mRxEventBus);
    }

    @Override
    public void attachView(SearchMvpView mvpView) {
        super.attachView(mvpView);

        subscribeOnSearchEvents();

        getMvpView().onItemClicked()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Article>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(Article article) {
                        getMvpView().showDetail(article);
                    }
                });
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    PublishRelay<CharSequence> getSearchProxy() {
        return mDataManager.getSearchProxy();
    }

    private void subscribeOnSearchEvents() {
        RxUtil.unsubscribe(searchSubscription);

//        searchSubscription = mDataManager.getSearchProxy()
//                .debounce(400, TimeUnit.MILLISECONDS)
//                .observeOn(AndroidSchedulers.mainThread())
//                .filter(charSequence -> !TextUtils.isEmpty(charSequence.toString()))
//                .subscribeOn(Schedulers.newThread())
//                .map(CharSequence::toString)
//                .subscribe(new Subscriber<String>() {
//                    @Override
//                    public void onCompleted() {
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                    }
//
//                    @Override
//                    public void onNext(String s) {
//                        loadItems(s);
//                    }
//                });


    }

    void loadItems(String search) {
        mDataManager.search(search)
                .map(SearchResponse::getArticle)
               .subscribe(new LoadingSubscriber<>());
    }

}
