package eu.proexe.android.mpp.ui.search;

import java.util.List;

import eu.proexe.android.mpp.data.model.response.Article;
import eu.proexe.android.mpp.ui.base.LoadingMvpView;
import rx.Observable;

/**
 * Created by Michał Trętowicz  on 18.03.17.
 */

public interface SearchMvpView extends LoadingMvpView<List<Article>> {

    Observable<Article> onItemClicked();

    void showDetail(Article article);
}
