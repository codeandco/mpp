package eu.proexe.android.mpp.data.model.response.video;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by Michał Trętowicz  on 09.02.17.
 */
@Root(name = "AllInitiatives")
public class Video {

    @Element(name = "PKey", required = false)
    public String pkey;

    @Element(name = "ImageName", required = false)
    public String imageName;

    @Element(name = "VideoName", required = false)
    public String VideoName;

    @Element(name = "Description", required = false)
    public String Description;

    @Element(name = "Heading", required = false)
    public String Heading;

    @Element(name = "SortOrder", required = false)
    public Integer SortOrder;

    @Element(name = "CreatedDate", required = false)
    public String CreatedDate;

    @Element(name = "CreatedUser", required = false)
    public String CreatedUser;

    @Element(name = "LastEditDate", required = false)
    public String LastEditDate;

    @Element(name = "LastEditUser", required = false)
    public String LastEditUser;


    @Override
    public String toString() {
        return "Video{" +
                "pkey='" + pkey + '\'' +
                ", imageName='" + imageName + '\'' +
                ", VideoName='" + VideoName + '\'' +
                ", Description='" + Description + '\'' +
                ", Heading='" + Heading + '\'' +
                ", SortOrder=" + SortOrder +
                ", CreatedDate='" + CreatedDate + '\'' +
                ", CreatedUser='" + CreatedUser + '\'' +
                ", LastEditDate='" + LastEditDate + '\'' +
                ", LastEditUser='" + LastEditUser + '\'' +
                '}';
    }
}
