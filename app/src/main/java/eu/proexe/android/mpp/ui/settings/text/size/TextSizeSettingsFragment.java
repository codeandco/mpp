package eu.proexe.android.mpp.ui.settings.text.size;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.Unbinder;
import eu.proexe.android.mpp.R;
import eu.proexe.android.mpp.data.local.PreferencesHelper;
import eu.proexe.android.mpp.ui.base.BaseActivity;
import io.apptik.widget.MultiSlider;

/**
 * Created by Michał Trętowicz  on 12.03.17.
 */

public class TextSizeSettingsFragment extends Fragment implements TextSizeSettingsMvpView {

    @Inject
    TextSizeSettingsPresenter presenter;

    @BindView(R.id.example_text)
    TextView exampleText;

    @BindView(R.id.use_system_size)
    SwitchCompat useSystemSize;

    @BindView(R.id.seekBar)
    MultiSlider seekBar;

    private Unbinder bind;


    @OnCheckedChanged(R.id.use_system_size)
    void useSystemSizeChanged(boolean checked) {
        presenter.useSystemSizeChanged(checked);

        if (checked) {
            seekBar.getThumb(0).setValue(0);
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflated = inflater.inflate(R.layout.fragment_settings_text_size, container, false);
        bind = ButterKnife.bind(this, inflated);
        return inflated;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter.attachView(this);


        seekBar.getThumb(0).setThumb(ContextCompat.getDrawable(getActivity(), R.drawable.thumb));
        seekBar.getThumb(0).setMax(28);
        seekBar.setMin(0);
        seekBar.setMax(30, false, false);


        seekBar.setOnThumbValueChangeListener((multiSlider, thumb, thumbIndex, value) -> {
            if (value > 0 && useSystemSize.isChecked()) {
                useSystemSize.setChecked(false);
            }

            if (value == 0) {
                useSystemSize.setChecked(true);
            }


            presenter.progressChanged(value);

            exampleText.setTextSize(PreferencesHelper.DEFAULT_TEXT_SIZE
                    + value / 5);
            presenter.textSizeChanged(exampleText.getTextSize());
        });
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((BaseActivity) getActivity()).activityComponent().inject(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        bind.unbind();
        presenter.detachView();
    }


    @Override
    public void showUseSystemSize(boolean systemSize) {
        useSystemSize.setChecked(systemSize);
    }

    @Override
    public void showTextSize(float textSize) {
        exampleText.setTextSize(textSize);
    }

    @Override
    public void showProgress(int progress) {
        seekBar.getThumb(0).setValue(progress);
    }

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.explanation)
    TextView explanation;

    @Override
    public void setTextSize(float textSize) {
        title.setTextSize(textSize);
        explanation.setTextSize(textSize);
    }
}
