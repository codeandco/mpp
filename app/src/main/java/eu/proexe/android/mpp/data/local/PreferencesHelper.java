package eu.proexe.android.mpp.data.local;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

import eu.proexe.android.mpp.data.event.ChangeTextSize;
import eu.proexe.android.mpp.injection.ApplicationContext;
import eu.proexe.android.mpp.util.RxEventBus;

@Singleton
public class PreferencesHelper {


    public static final int DEFAULT_TEXT_SIZE = 14;

    private static final String PREF_FILE_NAME = "android_application_pref_file";
    private static final String KEY_SYSTEM_SIZE = "key_system_size";
    private static final String KEY_NOTIFICATIONS_AT_NIGHT = "key_notifications_at_night";
    private static final String KEY_TEXT_SIZE = "key_text_size";
    private static final String KEY_PROGRESS = "key_progress";

    private final SharedPreferences mPref;
    private final Context context;
    private final RxEventBus rxEventBus;

    @Inject
    public PreferencesHelper(@ApplicationContext Context context, RxEventBus rxEventBus) {
        mPref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        this.context = context;
        this.rxEventBus = rxEventBus;
    }

    public void clear() {
        mPref.edit().clear().apply();
    }

    public boolean getUseSystemSize() {
        return mPref.getBoolean(KEY_SYSTEM_SIZE, true);
    }

    public void setUseSystemSize(boolean useSystemSize) {
        mPref.edit().putBoolean(KEY_SYSTEM_SIZE, useSystemSize).apply();
    }


    public boolean getNotificationsAtNight() {
        return mPref.getBoolean(KEY_NOTIFICATIONS_AT_NIGHT, false);
    }

    public void setNotificationsAtNight(boolean notificationsAtNight) {
        mPref.edit().putBoolean(KEY_NOTIFICATIONS_AT_NIGHT, notificationsAtNight).apply();
    }

    public float getTextSize() {
        return mPref.getFloat(KEY_TEXT_SIZE, DEFAULT_TEXT_SIZE);
    }

    public void setTextSize(float textSize) {

        mPref.edit().putFloat(KEY_TEXT_SIZE, convertPxToSp(textSize, context)).apply();

        rxEventBus.post(new ChangeTextSize(getTextSize()));
    }

    public float convertPxToSp(float px, Context context) {
        float sp = px / context.getResources().getDisplayMetrics().scaledDensity;
        return sp;
    }

    public int getProgress() {
        return mPref.getInt(KEY_PROGRESS, 0);
    }

    public void setProgress(int progress) {
        mPref.edit().putInt(KEY_PROGRESS, progress).apply();
    }
}
