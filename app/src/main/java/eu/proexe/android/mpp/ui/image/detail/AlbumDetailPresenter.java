package eu.proexe.android.mpp.ui.image.detail;

import java.util.List;

import javax.inject.Inject;

import eu.proexe.android.mpp.data.DataManager;
import eu.proexe.android.mpp.data.model.response.album.AlbumDetailItem;
import eu.proexe.android.mpp.ui.base.LoadingPresenter;
import eu.proexe.android.mpp.util.RxEventBus;
import eu.proexe.android.mpp.util.RxUtil;
import rx.Subscription;

/**
 * Created by Michał Trętowicz  on 20.02.17.
 */

public class AlbumDetailPresenter extends LoadingPresenter<AlbumDetailMvpView> {

    private Subscription subscription;

    @Inject
    public AlbumDetailPresenter(DataManager mDataManager, RxEventBus mRxEventBus) {
        super(mDataManager, mRxEventBus);
    }

    @Override
    public void attachView(AlbumDetailMvpView mvpView) {
        super.attachView(mvpView);
    }

    public void loadDetails(String pKey) {

        RxUtil.unsubscribe(subscription);
        subscription = mDataManager.getImagesByAlbum(pKey)
                .subscribe(new LoadingSubscriber<List<AlbumDetailItem>>());
    }

    @Override
    public void detachView() {
        super.detachView();
        RxUtil.unsubscribe(subscription);
    }

    public void back() {

//        mRxEventBus.post(EventCloseAlbumDetail.create());
    }
}
