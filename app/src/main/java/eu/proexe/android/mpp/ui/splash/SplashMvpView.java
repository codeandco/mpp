package eu.proexe.android.mpp.ui.splash;

import eu.proexe.android.mpp.ui.base.MvpView;

/**
 * Created by Michał Trętowicz  on 07.02.17.
 */

public interface SplashMvpView extends MvpView {
    void endSplash();

}
