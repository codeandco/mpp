package eu.proexe.android.mpp.data.event;

import eu.proexe.android.mpp.data.model.response.category.NewsCategory;

/**
 * Created by Michał Trętowicz  on 18.03.17.
 */

public class EventShowTab {

    private NewsCategory category;

    public NewsCategory getCategory() {
        return category;
    }

    public EventShowTab(NewsCategory name) {
        this.category = name;
    }
}
