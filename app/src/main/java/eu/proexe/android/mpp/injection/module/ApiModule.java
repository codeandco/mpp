package eu.proexe.android.mpp.injection.module;

import org.simpleframework.xml.convert.AnnotationStrategy;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.strategy.Strategy;

import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import eu.proexe.android.mpp.BuildConfig;
import eu.proexe.android.mpp.data.remote.ApiService;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * Created by mr on 24.11.2016.
 */

@Module
public class ApiModule {

    @Provides
    OkHttpClient provideOkHttpClient() {
        HttpLoggingInterceptor logger = new HttpLoggingInterceptor();
        logger.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY
                : HttpLoggingInterceptor.Level.NONE);


        return new OkHttpClient.Builder()
                .addInterceptor(logger)
                .connectTimeout(20_000, TimeUnit.MILLISECONDS)
                .readTimeout(20_000, TimeUnit.MILLISECONDS)
                .writeTimeout(20_000, TimeUnit.MILLISECONDS).build();
    }

    @Provides
    Retrofit provideRetrofit(OkHttpClient client) {

        Strategy strategy = new AnnotationStrategy();
        Persister serializer = new Persister(strategy);

        return new Retrofit.Builder()
                .baseUrl(ApiService.ENDPOINT)
                .client(client)
                .validateEagerly(true)
                .addConverterFactory(SimpleXmlConverterFactory.createNonStrict(serializer))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    @Provides
    ApiService provideApiService(Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }
}
