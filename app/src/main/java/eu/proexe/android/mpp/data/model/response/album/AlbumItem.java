package eu.proexe.android.mpp.data.model.response.album;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by Michał Trętowicz  on 09.02.17.
 */
@Root(name = "AllInitiatives")
public class AlbumItem {

    @Element(name = "Pkey", required = false)
    public String pKey;
    @Element(name = "AlbumName", required = false)
    public String albumName;
    @Element(name = "Year", required = false)
    public Integer year;
    @Element(name = "ImageName", required = false)
    public String imageName;
    @Element(name = "SortOrder", required = false)
    public Integer sortOrder;
    @Element(name = "ImageFullPath", required = false)
    public String imageFullPath;

    public String getPKey() {
        return pKey;
    }

    public void setPKey(String pKey) {
        this.pKey = pKey;
    }
}
