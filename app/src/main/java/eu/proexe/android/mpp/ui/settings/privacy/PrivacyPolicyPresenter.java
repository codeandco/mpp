package eu.proexe.android.mpp.ui.settings.privacy;

import javax.inject.Inject;

import eu.proexe.android.mpp.data.DataManager;
import eu.proexe.android.mpp.ui.base.BasePresenter;
import eu.proexe.android.mpp.util.RxEventBus;

/**
 * Created by Michał Trętowicz  on 12.03.17.
 */

public class PrivacyPolicyPresenter extends BasePresenter<PrivacyPolicyMvpView> {

    @Inject
    public PrivacyPolicyPresenter(DataManager mDataManager, RxEventBus mRxEventBus) {
        super(mDataManager, mRxEventBus);
    }

    @Override
    public void attachView(PrivacyPolicyMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }
}
