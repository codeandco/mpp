package eu.proexe.android.mpp.data.model.request;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

/**
 * Created by Shoeb on 3/7/17.
 */

@Namespace(reference = "http://tempuri.org/")
@Root(name = "PushNotifications")
public class AddDeviceToken {

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    @Element(name = "deviceToken")
    private String deviceToken = "";

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    @Element(name = "deviceType")
    private String deviceType = "ANDROID";

}
