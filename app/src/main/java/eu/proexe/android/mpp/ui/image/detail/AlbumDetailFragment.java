package eu.proexe.android.mpp.ui.image.detail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.joda.time.DateTime;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import eu.proexe.android.mpp.R;
import eu.proexe.android.mpp.data.model.response.album.AlbumDetailItem;
import eu.proexe.android.mpp.ui.base.BaseActivity;
import eu.proexe.android.mpp.ui.main.MainActivity;
import timber.log.Timber;

/**
 * Created by Michał Trętowicz  on 20.02.17.
 */

public class AlbumDetailFragment extends Fragment implements AlbumDetailMvpView {

    private static final String EXTRA_KEY = "pKey";
    private static final String EXTRA_KEY_YEAR = "EXTRA_KEY_YEAR";
    public static HashMap<String, List<AlbumDetailItem>> sCache = new HashMap<>();
    @Inject
    AlbumDetailPresenter presenter;
    @BindView(R.id.list)
    ViewPager list;
    @BindView(R.id.progress)
    View progress;
    @Inject
    AlbumDetailAdapter adapter;
    @BindView(R.id.year)
    TextView year;
    @BindView(R.id.page)
    TextView page;
    private Unbinder bind;

    public static AlbumDetailFragment newInstance(String pKey, int year, String name) {
        AlbumDetailFragment fragment = new AlbumDetailFragment();
        Bundle args = new Bundle();
        args.putString(EXTRA_KEY, pKey);
        args.putInt(EXTRA_KEY_YEAR, year);
        args.putString("name", name);
        fragment.setArguments(args);
        return fragment;
    }

    String getExtraKey() {
        return getArguments().getString(EXTRA_KEY);
    }

    int getYear() {
        return getArguments().getInt(EXTRA_KEY_YEAR, DateTime.now().getYear());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflated = inflater.inflate(R.layout.fragment_album_detail, container, false);
        bind = ButterKnife.bind(this, inflated);
        return inflated;
    }

    @OnClick(R.id.back)
    void backClicked() {
        MainActivity.closeAlbum = true;
        getActivity().onBackPressed();
//        presenter.back();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter.attachView(this);
        String extraKey = getExtraKey();
        Timber.d("extraKey " + extraKey);

        if(sCache.containsKey(extraKey) ) {

            showItems(sCache.get(extraKey));
        } else {
            presenter.loadDetails(extraKey);
        }

        list.setAdapter(adapter);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((BaseActivity) getActivity()).activityComponent().inject(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        bind.unbind();
        presenter.detachView();
    }

    @Override
    public void showLoading(boolean show) {
        progress.animate().alpha(show ? 1 : 0).setDuration(250);
    }

    @Override
    public void showErrorLoading() {
    }

    @Override
    public void showItems(final List<AlbumDetailItem> data) {
        Timber.d("details loaded " + data);

        sCache.put(getExtraKey(), data);

//        year.setText(getYear() + "");
        year.setText(getArguments().getString("name", year + ""));

        list.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);

                //todo refactor to string
                page.setText("Photo " + (position + 1) + " of " + data.size());
            }
        });

        page.setText("Photo 1 of " + data.size());
        adapter.setItems(data);
    }

    @Override
    public void showEmpty() {
    }

    @Override
    public void hideEmpty() {
    }


    @Override
    public void setTextSize(float textSize) {
        year.setTextSize(textSize);
        page.setTextSize(textSize);
    }
}
