package eu.proexe.android.mpp.ui.base;


public interface LoadingMvpView<T> extends MvpView {

    void showLoading(boolean show);

    void showErrorLoading();

    void showItems(T data);

    void showEmpty();

    void hideEmpty();
}
