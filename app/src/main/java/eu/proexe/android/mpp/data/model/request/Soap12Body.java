package eu.proexe.android.mpp.data.model.request;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "soap12:Body")
public class Soap12Body {

    @Element(name = "GetCategoryWiseNews", required = false)
    private GetCategoryWiseNews getCategoryWiseNews;

    @Element(name = "GetVideos", required = false)
    private GetVideos getVideos;

    @Element(name = "GetImagesbyAlbum", required = false)
    private GetImagesbyAlbum getImagesbyAlbum;

    @Element(name = "GetAlbums", required = false)
    private GetAlbums getAlbums;

    @Element(name = "GetAllCategories", required = false)
    private GetAllCategories getAllCategories;

    public GetNewsDescriptionByID getGetNewsDescriptionByID() {
        return getNewsDescriptionByID;
    }

    @Element(name = "GetNewsDescriptionByID", required = false)
    private GetNewsDescriptionByID getNewsDescriptionByID;

    @Element(name = "GetSearchNews", required = false)
    private GetSearchNews getSearchNews;

    public AddDeviceToken getmAddDeviceToken() {
        return mAddDeviceToken;
    }

    public void setmAddDeviceToken(AddDeviceToken mAddDeviceToken) {
        this.mAddDeviceToken = mAddDeviceToken;
    }

    @Element(name = "RegisterUserDevice", required = false)
    private AddDeviceToken mAddDeviceToken;

    public void setGetSearchNews(GetSearchNews getSearchNews) {
        this.getSearchNews = getSearchNews;
    }

    public void setGetNewsDescriptionByID(GetNewsDescriptionByID getNewsDescriptionByID) {
        this.getNewsDescriptionByID = getNewsDescriptionByID;
    }

    public void setGetImagesbyAlbum(GetImagesbyAlbum getImagesbyAlbum) {
        this.getImagesbyAlbum = getImagesbyAlbum;
    }

    public void setGetAlbums(GetAlbums getAlbums) {
        this.getAlbums = getAlbums;
    }

    public void setGetAllCategories(GetAllCategories getAllCategories) {
        this.getAllCategories = getAllCategories;
    }

    public void setGetVideos(GetVideos getVideos) {
        this.getVideos = getVideos;
    }

    public GetCategoryWiseNews getGetCategoryWiseNews() {
        return getCategoryWiseNews;
    }

    public void setGetCategoryWiseNews(GetCategoryWiseNews getCategoryWiseNews) {
        this.getCategoryWiseNews = getCategoryWiseNews;
    }
}