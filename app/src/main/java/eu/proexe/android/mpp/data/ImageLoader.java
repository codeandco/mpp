package eu.proexe.android.mpp.data;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import javax.inject.Inject;
import javax.inject.Singleton;

import eu.proexe.android.mpp.BuildConfig;
import eu.proexe.android.mpp.injection.ApplicationContext;
import timber.log.Timber;

/**
 * Created by Maciej Różański on 29.12.2016.
 */

@Singleton
public class ImageLoader {

    private final Context context;
    private final Picasso picasso;

    @Inject
    public ImageLoader(@ApplicationContext Context context) {
        this.context = context;

        Picasso.Builder builder = new Picasso.Builder(context);
        builder.listener((picasso1, uri, exception) -> Timber.d("Picasso failed: " + uri));

        this.picasso = builder.build();
        picasso.setLoggingEnabled(BuildConfig.DEBUG);
        picasso.setIndicatorsEnabled(BuildConfig.DEBUG);
    }


    public void load(ImageView view, String url) {
        picasso
                .load(url)
                .fit()
                .centerCrop()
                .into(view);
    }

    public void load(ImageView view, String url, Transformation transformation) {
        picasso
                .load(url)
                .fit()
                .transform(transformation)
                .centerCrop()
                .into(view);
    }


    public void load(ImageView view, String url, Callback callback) {
        picasso
                .load(url)
                .fit()
                .centerCrop()
                .into(view, callback);
    }

    public void load(ImageView view, @DrawableRes int res) {
        picasso
                .load(res)
                .fit()
                .centerCrop()
                .into(view);
    }


    public void load(ImageView view, String url, @DrawableRes int placeholder) {
        picasso
                .load(url)
                .fit()
                .centerCrop()
                .placeholder(placeholder)
                .into(view);
    }

    public void loadNoFade(ImageView view, String url, @DrawableRes int placeholder) {
        picasso
                .load(url)
                .fit()
                .noFade()
                .centerCrop()
                .placeholder(placeholder)
                .into(view);
    }


    public void loadFit(ImageView view, String url) {
        picasso
                .load(url)
                .fit()
                .centerInside()
                .into(view);
    }

    public void loadFit(ImageView view, @DrawableRes int res) {
        picasso
                .load(res)
                .fit()
                .centerInside()
                .into(view);
    }
}
