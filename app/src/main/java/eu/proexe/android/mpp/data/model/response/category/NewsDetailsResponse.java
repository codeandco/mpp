package eu.proexe.android.mpp.data.model.response.category;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Michał Trętowicz  on 09.02.17.
 */

@Root(name = "soap:Envelope")
public class NewsDetailsResponse {

    @Path("Body/GetNewsDescriptionByIDResponse/GetNewsDescriptionByIDResult/diffgram/NewDataSet")
    @ElementList(inline = true)
    private List<NewsDetails> newsDetailsList;

    public List<NewsDetails> getNewsDetailsList() {
        return newsDetailsList;
    }
}
