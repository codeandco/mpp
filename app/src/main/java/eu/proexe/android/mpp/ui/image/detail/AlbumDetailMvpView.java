package eu.proexe.android.mpp.ui.image.detail;

import java.util.List;

import eu.proexe.android.mpp.data.model.response.album.AlbumDetailItem;
import eu.proexe.android.mpp.ui.base.LoadingMvpView;

/**
 * Created by Michał Trętowicz  on 20.02.17.
 */

public interface AlbumDetailMvpView extends LoadingMvpView<List<AlbumDetailItem>> {
}
