package eu.proexe.android.mpp.ui.news.list;

import java.util.List;

import eu.proexe.android.mpp.data.model.response.Article;
import eu.proexe.android.mpp.ui.base.LoadingMvpView;
import rx.Observable;


public interface NewsListMvpView extends LoadingMvpView<List<Article>> {

    Observable<Article> articleClicked();

    void showArticleDetail(Article article);
}
