package eu.proexe.android.mpp.ui.widgets;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import eu.proexe.android.mpp.data.local.PreferencesHelper;

/**
 * Created by Michał Trętowicz  on 15.03.17.
 */

public class TextSizeAwareTextView extends AppCompatTextView {


    public float progress = PreferencesHelper.DEFAULT_TEXT_SIZE;

    public TextSizeAwareTextView(Context context) {
        super(context);
    }

    public TextSizeAwareTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TextSizeAwareTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void increaseTextSize() {
        if(progress ==  PreferencesHelper.DEFAULT_TEXT_SIZE + 6) {
            return;
        }
        progress++;
        setTextSize(progress);
    }

    public void decreaseTextSize() {

        if (progress == PreferencesHelper.DEFAULT_TEXT_SIZE) {
            return;
        }

        progress--;
        setTextSize(progress);
    }
}
