package eu.proexe.android.mpp.data.model.response.category;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Michał Trętowicz  on 09.02.17.
 */

@Root(name = "soap:Envelope")
public class NewsCategoriesResponse {

    @Path("Body/GetAllCategoriesResponse/GetAllCategoriesResult/diffgram/NewDataSet")
    @ElementList(inline = true)
    private List<NewsCategory> categories;

    public List<NewsCategory> getCategories() {
        return categories;
    }
}
