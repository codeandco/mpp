package eu.proexe.android.mpp.ui.news.details;

import javax.inject.Inject;

import eu.proexe.android.mpp.data.DataManager;
import eu.proexe.android.mpp.ui.base.LoadingPresenter;
import eu.proexe.android.mpp.util.RxEventBus;
import eu.proexe.android.mpp.util.RxUtil;
import rx.Subscription;

/**
 * Created by Michał Trętowicz  on 12.02.17.
 */

public class NewsDetailsPresenter extends LoadingPresenter<NewsDetailsMvpView> {

    private Subscription subscription;

    @Inject
    public NewsDetailsPresenter(DataManager mDataManager, RxEventBus mRxEventBus) {
        super(mDataManager, mRxEventBus);
    }

    @Override
    public void attachView(NewsDetailsMvpView mvpView) {
        super.attachView(mvpView);

        float textSize = mDataManager.getPreferencesHelper().getTextSize();
        getMvpView().showTextSize(textSize);


    }


    public void loadDetails(String pKey) {
        RxUtil.unsubscribe(subscription);

        subscription = mDataManager.getNewsDescription(pKey)
                .map(newsDetailsResponse -> newsDetailsResponse.getNewsDetailsList().get(0))
                .subscribe(new SingleValueLoadingSubscriber<>());
    }

    @Override
    public void detachView() {
        super.detachView();
        RxUtil.unsubscribe(subscription);
    }
}
