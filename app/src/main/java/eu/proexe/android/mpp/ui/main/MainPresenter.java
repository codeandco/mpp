package eu.proexe.android.mpp.ui.main;

import android.content.Intent;

import javax.inject.Inject;

import eu.proexe.android.mpp.data.DataManager;
import eu.proexe.android.mpp.data.EventCloseAlbumDetail;
import eu.proexe.android.mpp.data.event.EventShowTab;
import eu.proexe.android.mpp.data.event.OnActivityResultEvent;
import eu.proexe.android.mpp.injection.ConfigPersistent;
import eu.proexe.android.mpp.ui.base.BasePresenter;
import eu.proexe.android.mpp.util.RxEventBus;
import eu.proexe.android.mpp.util.RxUtil;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

@ConfigPersistent
public class MainPresenter extends BasePresenter<MainMvpView> {

    private Subscription searchSub;

    @Inject
    public MainPresenter(DataManager mDataManager, RxEventBus mRxEventBus) {
        super(mDataManager, mRxEventBus);
    }


    @Override
    public void attachView(MainMvpView mvpView) {
        super.attachView(mvpView);


        RxUtil.unsubscribe(searchSub);
        searchSub = mRxEventBus.filteredObservable(EventShowTab.class)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<EventShowTab>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(EventShowTab eventShowTab) {
                        Timber.d("show tab" );
                        getMvpView().showTab(eventShowTab);
                    }
                });
    }

    @Override
    public void detachView() {
        super.detachView();
        RxUtil.unsubscribe(searchSub);

    }

    public void closeAlbumDetail() {
        mRxEventBus.post(EventCloseAlbumDetail.create());
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mRxEventBus.post(new OnActivityResultEvent(requestCode, resultCode, data));
    }
}
