package eu.proexe.android.mpp.ui.news.tabs;


import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import eu.proexe.android.mpp.data.DataManager;
import eu.proexe.android.mpp.data.event.EventShowTab;
import eu.proexe.android.mpp.data.event.EventTabs;
import eu.proexe.android.mpp.data.model.request.AddDeviceToken;
import eu.proexe.android.mpp.data.model.response.category.NewsCategory;
import eu.proexe.android.mpp.ui.base.LoadingPresenter;
import eu.proexe.android.mpp.util.RxEventBus;
import eu.proexe.android.mpp.util.RxUtil;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

public class NewsTabsPresenter extends LoadingPresenter<NewsTabsMvpView> {

    private static final String TAG = NewsTabsPresenter.class.getSimpleName();
    private Subscription categoriesSub;
    private Subscription addDeviceTokenSub;
    private Subscription showTabSub;

    @Inject
    public NewsTabsPresenter(DataManager mDataManager, RxEventBus mRxEventBus) {
        super(mDataManager, mRxEventBus);
    }

    @Override
    public void attachView(NewsTabsMvpView mvpView) {
        super.attachView(mvpView);

        RxUtil.unsubscribe(showTabSub);
        showTabSub = mRxEventBus.filteredObservable(EventShowTab.class)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<EventShowTab>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(EventShowTab eventShowTab) {
                        Timber.d("show tab");

                        getMvpView().showTabWithName(eventShowTab.getCategory());
                    }
                });
    }

    @Override
    public void detachView() {
        super.detachView();
        RxUtil.unsubscribe(addDeviceTokenSub);
        RxUtil.unsubscribe(categoriesSub);
        RxUtil.unsubscribe(showTabSub);
    }

    public void load(ArrayList<String> extra) {


        System.out.println("getAllCategories Called ");
        RxUtil.unsubscribe(addDeviceTokenSub);

        // Get token
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG,"Device Token : " + token);

        addDeviceTokenSub = mDataManager.addDeviceToken(token).subscribe(new Subscriber<AddDeviceToken>() {
            @Override
            public void onCompleted() {
                Log.e(TAG,"onCompleted called ");

            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG,"onError : " + e.getMessage());
                e.printStackTrace();


            }

            @Override
            public void onNext(AddDeviceToken addDeviceToken) {
                Log.e(TAG,"onNext : " + addDeviceToken);

            }
        });

        RxUtil.unsubscribe(categoriesSub);
        categoriesSub = mDataManager.getAllCategories()
                .subscribe(new LoadingSubscriber<List<NewsCategory>>() {

                    @Override
                    public void onNext(List<NewsCategory> orders) {

                        Timber.d("extra " + extra);
                        Timber.d("*************");
                        for (NewsCategory order : orders) {
                            Timber.d("" + order.NewsTypeID);

                        }

                        Timber.d("*************");

                        List<NewsCategory> res = new ArrayList<>();
                        for (NewsCategory order : orders) {


                            if (order.NewsType.equals("Breaking News")) {
                                order.setNewsType("Interviews");
                            }


                            if (!order.NewsType.equals("CompanyNews") && extra.contains(order.NewsTypeID)) {
                                int index = extra.indexOf(order.NewsTypeID);
                                Timber.d("add " + index + " " + order.NewsTypeID);
                                res.add(order);
                            }


//                            Timber.d("category " + order.NewsType + " " + order.NewsTypeID);
                        }
                        try {
                            if (extra.contains("wat")) {
                                Collections.swap(res, 0, 1);
                            } else {
                                Collections.swap(res, 0, 2);
                            }

                        } catch (Throwable ignored) {

                        }

//                        res.removeAll(Collections.<NewsCategory>singleton(null));


                        super.onNext(res);
                    }
                });
    }

    public void showCategories(List<NewsCategory> names) {
        mRxEventBus.post(new EventTabs(names));
    }

    public void refreshTextSize() {
        getMvpView().setTextSize(mDataManager.getPreferencesHelper().getTextSize());
    }
}
