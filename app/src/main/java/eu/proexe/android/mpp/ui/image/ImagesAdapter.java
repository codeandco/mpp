package eu.proexe.android.mpp.ui.image;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jakewharton.rxrelay.PublishRelay;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import eu.proexe.android.mpp.R;
import eu.proexe.android.mpp.data.ImageLoader;
import eu.proexe.android.mpp.data.local.PreferencesHelper;
import eu.proexe.android.mpp.data.model.response.album.AlbumItem;
import eu.proexe.android.mpp.injection.ActivityContext;
import eu.proexe.android.mpp.ui.main.MainActivity;
import eu.proexe.android.mpp.util.MaskTransformation;
import rx.Observable;

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ViewHolder> {

    private final LayoutInflater layoutInflater;
    private final ImageLoader imageLoader;
    private List<AlbumItem> items = new ArrayList<>();

    private PublishRelay<AlbumItem> clickedRelay = PublishRelay.create();

    @Inject
    public ImagesAdapter(@ActivityContext LayoutInflater layoutInflater,
                         ImageLoader imageLoader) {
        this.layoutInflater = layoutInflater;
        this.imageLoader = imageLoader;
    }

    private float textSize = PreferencesHelper.DEFAULT_TEXT_SIZE;

    public void setTextSize(float textSize) {
        this.textSize = textSize;
        notifyDataSetChanged();
    }


    Observable<AlbumItem> getClickedRelay() {
        return clickedRelay.asObservable();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.item_album, parent, false));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    void setItems(List<AlbumItem> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setData(items.get(position));
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        AlbumItem item;

        @BindView(R.id.image1)
        ImageView image1;

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.date)
        TextView date;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick({R.id.image1})
        void item1Clicked() {
            if (MainActivity.closeAlbum) {
                return;
            }

            if (item != null) {
                MainActivity.closeAlbum = true;
                clickedRelay.call(item);
            }
        }

        public void setData(AlbumItem item) {
            this.item = item;
            imageLoader.load(image1, item.imageFullPath,
                    new MaskTransformation(itemView.getContext(), R.drawable.mask));

            name.setText(item.albumName);
            date.setText(item.year + "");

            name.setTextSize(textSize);
            date.setTextSize(textSize);
        }
    }
}
