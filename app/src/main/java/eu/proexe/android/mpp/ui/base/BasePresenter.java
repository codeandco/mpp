package eu.proexe.android.mpp.ui.base;

import eu.proexe.android.mpp.data.DataManager;
import eu.proexe.android.mpp.data.event.ChangeTextSize;
import eu.proexe.android.mpp.util.RxEventBus;
import eu.proexe.android.mpp.util.RxUtil;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Base class that implements the Presenter interface and provides a base implementation for
 * attachView() and detachView(). It also handles keeping a reference to the mvpView that
 * can be accessed from the children classes by calling getMvpView().
 */
public class BasePresenter<T extends MvpView> implements Presenter<T> {

    protected final DataManager mDataManager;
    protected final RxEventBus mRxEventBus;
    private T mMvpView;
    private Subscription changeTextSizeSub;

    public BasePresenter(DataManager mDataManager,
                         RxEventBus mRxEventBus) {
        this.mDataManager = mDataManager;
        this.mRxEventBus = mRxEventBus;
    }

    @Override
    public void attachView(T mvpView) {
        mMvpView = mvpView;
        float textSize = mDataManager.getPreferencesHelper().getTextSize();
        getMvpView().setTextSize(textSize);

        RxUtil.unsubscribe(changeTextSizeSub);
        changeTextSizeSub = mRxEventBus.filteredObservable(ChangeTextSize.class)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ChangeTextSize>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(ChangeTextSize changeTextSize) {
                        getMvpView().setTextSize(changeTextSize.getTextSize());
                    }
                });
    }

    @Override
    public void detachView() {
        mMvpView = null;
        RxUtil.unsubscribe(changeTextSizeSub);
    }

    public boolean isViewAttached() {
        return mMvpView != null;
    }

    public T getMvpView() {
        return mMvpView;
    }

    public void checkViewAttached() {
        if (!isViewAttached()) throw new MvpViewNotAttachedException();
    }

    public static class MvpViewNotAttachedException extends RuntimeException {
        public MvpViewNotAttachedException() {
            super("Please call Presenter.attachView(MvpView) before" +
                    " requesting data to the Presenter");
        }
    }
}

