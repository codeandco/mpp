package eu.proexe.android.mpp.pushnotification.fcm;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Random;

import eu.proexe.android.mpp.R;
import eu.proexe.android.mpp.data.modals.PushNotificationsModal;
import eu.proexe.android.mpp.data.model.response.Article;
import eu.proexe.android.mpp.ui.news.details.NewsDetailsActivity;
import me.leolin.shortcutbadger.ShortcutBadger;

import static eu.proexe.android.mpp.ui.news.details.NewsDetailsActivity.KEY_ARTICLE;
import static eu.proexe.android.mpp.ui.news.details.NewsDetailsActivity.KEY_NEWS_TYPE_ID;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());



        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
                scheduleJob();
            } else {
                // Handle message within 10 seconds
                handleNow();
            }

        }
//        Gson mGson = new Gson();
//        PushNotificationsModal mPushNotificationsModal = mGson.fromJson(remoteMessage.getData().toString(),PushNotificationsModal.class);
        PushNotificationsModal mPushNotificationsModal = new PushNotificationsModal();

        mPushNotificationsModal.setPkey(remoteMessage.getData().get("pkey"));
        mPushNotificationsModal.setMessage(remoteMessage.getData().get("message"));
        mPushNotificationsModal.setNewsTypeId(remoteMessage.getData().get("NewsTypeID"));

        try {
            if (remoteMessage.getData().containsKey("badge")){
                String badgeCount = remoteMessage.getData().get("badge");
                ShortcutBadger.applyCount(this, Integer.parseInt(badgeCount)); //for 1.1.4+
            }else{
                ShortcutBadger.applyCount(this, 1); //for 1.1.4+

            }

        }catch (Exception e){
            e.printStackTrace();

            ShortcutBadger.applyCount(this, 1); //for 1.1.4+
        }

        sendNotification(mPushNotificationsModal);

/*
        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            sendNotification(remoteMessage.getNotification().getBody());
        }*/


        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    /**
     * Schedule a job using FirebaseJobDispatcher.
     */
    private void scheduleJob() {
        // [START dispatch_job]
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        Job myJob = dispatcher.newJobBuilder()
                .setService(MyJobService.class)
                .setTag("my-job-tag")
                .build();
        dispatcher.schedule(myJob);
        // [END dispatch_job]
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param bean FCM message body received.
     */
    private void sendNotification(PushNotificationsModal bean) {
        Intent intent = new Intent(this, NewsDetailsActivity.class);
        Article article = new Article();
        article.setPKey(bean.getPkey());
        intent.putExtra(KEY_ARTICLE, article);
        intent.putExtra(KEY_NEWS_TYPE_ID, bean.getNewsTypeId());
        intent.putExtra("isFullImagePath", false);
        intent.putExtra("FROM_NOTIFICATION", true);
        intent.putExtra("PushNotificationsModal", bean);
        intent.putExtra("fullImagePath", "");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("MPP Notification")
                .setContentText(bean.getMessage())
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(randomNumber() /* ID of notification */, notificationBuilder.build());
    }

    private int randomNumber(){

        Random rand = new Random();

        return rand.nextInt(500) + 1;
    }
}