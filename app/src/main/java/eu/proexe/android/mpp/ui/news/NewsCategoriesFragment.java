package eu.proexe.android.mpp.ui.news;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Arrays;

import javax.inject.Inject;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import eu.proexe.android.mpp.R;
import eu.proexe.android.mpp.data.model.response.category.NewsCategory;
import eu.proexe.android.mpp.ui.base.BaseActivity;
import eu.proexe.android.mpp.ui.news.tabs.NewsTabsFragment;

/**
 * Created by Michał Trętowicz  on 18.03.17.
 */

public class NewsCategoriesFragment extends Fragment implements  NewsCategoriesMvpView {

    @BindView(R.id.pager)
    ViewPager pager;

    @BindView(R.id.latest)
    TextView latest;

    @BindView(R.id.news)
    TextView news;

    @BindColor(R.color.active_news_tab)
    int activeNewsTab;

    @BindColor(R.color.active_news_text)
    int activeNewsText;

    private Unbinder bind;

    @Inject
    NewsCategoriesPresenter presenter;

    NewsTabsFragment[] fragments = {
            NewsTabsFragment.newInstance(Arrays.asList("wat","jew")),
            NewsTabsFragment.newInstance(Arrays.asList("mn","ex","bn"))
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflated = inflater.inflate(R.layout.fragment_news_categories, container, false);
        bind = ButterKnife.bind(this, inflated);


        pager.setOffscreenPageLimit(6);

        latest.performClick();

        pager.setAdapter(new FragmentStatePagerAdapter(getChildFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return fragments[position];
            }

            @Override
            public int getCount() {
                return fragments.length;
            }
        });

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        latest.performClick();
                        break;

                    case 1:
                        news.performClick();
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        return inflated;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.attachView(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((BaseActivity) getActivity()).activityComponent().inject(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        bind.unbind();
        presenter.detachView();
    }

    @OnClick(R.id.latest)
    void latestClicked() {

        latest.setBackgroundColor(activeNewsTab);
        latest.setTextColor(activeNewsText);

        news.setTextColor(Color.WHITE);
        news.setBackgroundColor(Color.TRANSPARENT);

        pager.setCurrentItem(0);
    }

    @OnClick(R.id.news)
    void newsClicked() {
        news.setBackgroundColor(activeNewsTab);
        news.setTextColor(activeNewsText);

        latest.setTextColor(Color.WHITE);
        latest.setBackgroundColor(Color.TRANSPARENT);

        pager.setCurrentItem(1);
    }

    @Override
    public void showTabWithName(NewsCategory category) {
        if(Arrays.asList("jew","wat").contains(category.NewsTypeID)) {
            latest.performClick();
        } else {
            news.performClick();
        }
    }

    @Override
    public void setTextSize(float textSize) {
        latest.setTextSize(textSize);
        news.setTextSize(textSize);
    }
}
