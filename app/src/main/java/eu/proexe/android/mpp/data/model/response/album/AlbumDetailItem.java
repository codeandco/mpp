package eu.proexe.android.mpp.data.model.response.album;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by Michał Trętowicz  on 09.02.17.
 */
@Root(name = "AllInitiatives")
public class AlbumDetailItem {

    @Element(name = "PKey", required = false)
    public String pkey;

    @Element(name = "AlbumPkey", required = false)
    public String albumPKey;

    @Element(name = "AlbumName", required = false)
    public String albumName;

    @Element(name = "Year", required = false)
    public Integer year;

    @Element(name = "ImageName", required = false)
    public String imageName;

    @Element(name = "SortOrder", required = false)
    public Integer sortOrder;

    @Element(name = "ImageFullPath", required = false)
    public String imageFullPath;
}
