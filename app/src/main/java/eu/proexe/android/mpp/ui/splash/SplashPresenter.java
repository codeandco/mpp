package eu.proexe.android.mpp.ui.splash;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import eu.proexe.android.mpp.BuildConfig;
import eu.proexe.android.mpp.data.DataManager;
import eu.proexe.android.mpp.ui.base.BasePresenter;
import eu.proexe.android.mpp.util.RxEventBus;
import eu.proexe.android.mpp.util.RxUtil;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Michał Trętowicz  on 07.02.17.
 */

public class SplashPresenter extends BasePresenter<SplashMvpView> {

    private Subscription subscribe;

    @Inject
    public SplashPresenter(DataManager mDataManager, RxEventBus mRxEventBus) {
        super(mDataManager, mRxEventBus);
    }


    @Override
    public void attachView(SplashMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
        RxUtil.unsubscribe(subscribe);
    }

    public void showSplash() {
        RxUtil.unsubscribe(subscribe);

        subscribe = Observable.timer(BuildConfig.DEBUG ? 0 : 3, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Long>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(Long aLong) {
                        getMvpView().endSplash();
                    }
                });
    }
}
