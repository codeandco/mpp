package eu.proexe.android.mpp.data.event;

import java.util.ArrayList;
import java.util.List;

import eu.proexe.android.mpp.data.model.response.category.NewsCategory;

/**
 * Created by Michał Trętowicz  on 18.03.17.
 */

public class EventTabs {

    private  List<NewsCategory> tabsNames = new ArrayList<>();

    public List<NewsCategory> getTabsNames() {
        return tabsNames;
    }

    public EventTabs(List<NewsCategory> tabsNames) {
        this.tabsNames = tabsNames;
    }
}
