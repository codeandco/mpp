package eu.proexe.android.mpp.data.model.response.category;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by Michał Trętowicz  on 10.02.17.
 */
@Root(name = "AllInitiatives")
public class NewsDetails {

    @Element(name = "PKey", required = false)
    public String PKey;

    @Element(name = "NewsTypeID", required = false)
    public String NewsTypeID;

    @Element(name = "Header", required = false)
    public String Header;

    @Element(name = "NewsSDesc", required = false)
    public String NewsSDesc;

    @Element(name = "NewsDesc", required = false)
    public String NewsDesc;

    @Element(name = "ThumbNailName", required = false)
    public String ThumbNailName;

    @Element(name = "ThumbNailTitle", required = false)
    public String ThumbNailTitle;

    @Element(name = "ImageTitle", required = false)
    public String ImageTitle;

    @Element(name = "ImageName", required = false)
    public String ImageName;

    @Element(name = "Display", required = false)
    public Boolean Display;

    @Element(name = "StartDate", required = false)
    public String StartDate;

    @Element(name = "ExpiryDate", required = false)
    public String ExpiryDate;

    @Element(name = "IsLatest", required = false)
    public Boolean IsLatest;

    @Element(name = "SortOrder", required = false)
    public Integer SortOrder;

    @Element(name = "CreatedDate", required = false)
    public String CreatedDate;

    @Element(name = "CreatedUser", required = false)
    public String CreatedUser;

    @Element(name = "LastEditDate", required = false)
    public String LastEditDate;

    @Element(name = "LastEditUser", required = false)
    public String LastEditUser;

    @Element(name = "IsCelebrityNews", required = false)
    public Boolean IsCelebrityNews;

    @Element(name = "NewsType", required = false)
    public String NewsType;


    @Override
    public String toString() {
        return "NewsDetails{" +
                "PKey='" + PKey + '\'' +
                ", NewsTypeID='" + NewsTypeID + '\'' +
                ", Header='" + Header + '\'' +
                ", NewsSDesc='" + NewsSDesc + '\'' +
                ", NewsDesc='" + NewsDesc + '\'' +
                ", ThumbNailName='" + ThumbNailName + '\'' +
                ", ThumbNailTitle='" + ThumbNailTitle + '\'' +
                ", ImageTitle='" + ImageTitle + '\'' +
                ", ImageName='" + ImageName + '\'' +
                ", Display=" + Display +
                ", StartDate='" + StartDate + '\'' +
                ", ExpiryDate='" + ExpiryDate + '\'' +
                ", IsLatest=" + IsLatest +
                ", SortOrder=" + SortOrder +
                ", CreatedDate='" + CreatedDate + '\'' +
                ", CreatedUser='" + CreatedUser + '\'' +
                ", LastEditDate='" + LastEditDate + '\'' +
                ", LastEditUser='" + LastEditUser + '\'' +
                ", IsCelebrityNews=" + IsCelebrityNews +
                ", NewsType='" + NewsType + '\'' +
                '}';
    }
}
