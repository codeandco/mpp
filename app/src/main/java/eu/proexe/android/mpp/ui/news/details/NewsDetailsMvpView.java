package eu.proexe.android.mpp.ui.news.details;

import eu.proexe.android.mpp.data.model.response.category.NewsDetails;
import eu.proexe.android.mpp.ui.base.LoadingMvpView;

/**
 * Created by Michał Trętowicz  on 12.02.17.
 */

public interface NewsDetailsMvpView extends LoadingMvpView<NewsDetails> {
    void showTextSize(float textSize);

}
