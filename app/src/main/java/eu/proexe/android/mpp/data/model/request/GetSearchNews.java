package eu.proexe.android.mpp.data.model.request;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

/**
 * Created by Michał Trętowicz  on 18.03.17.
 */
@Namespace(reference = "http://tempuri.org/")
@Root(name = "GetSearchNews")
public class GetSearchNews {

    @Element(name = "strText")
    private String strText;

    public String getStrText() {
        return strText;
    }

    public void setStrText(String strText) {
        this.strText = strText;
    }
}
