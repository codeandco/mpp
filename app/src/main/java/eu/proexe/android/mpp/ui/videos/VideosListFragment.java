package eu.proexe.android.mpp.ui.videos;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import eu.proexe.android.mpp.R;
import eu.proexe.android.mpp.data.model.response.video.Video;
import eu.proexe.android.mpp.ui.base.BaseActivity;
import eu.proexe.android.mpp.util.VerticalSpaceItemDecoration;
import eu.proexe.android.mpp.util.ViewUtil;

public class VideosListFragment extends Fragment implements VideosListMvpView {


    @Inject
    VideosListPresenter presenter;
    @BindView(R.id.list)
    RecyclerView list;
    @Inject
    VideosAdapter adapter;
    @BindView(R.id.progress)
    View progress;
    private Unbinder bind;

    public static VideosListFragment newInstance() {
        VideosListFragment fragment = new VideosListFragment();
        fragment.setArguments(new Bundle());
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflated = inflater.inflate(R.layout.fragment_news_list, container, false);
        bind = ButterKnife.bind(this, inflated);
        return inflated;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        list.setLayoutManager(new LinearLayoutManager(getActivity()));
        list.setAdapter(adapter);
        list.addItemDecoration(new VerticalSpaceItemDecoration(ViewUtil.dpToPx(2)));

        presenter.attachView(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setRetainInstance(true);
        ((BaseActivity) getActivity()).activityComponent().inject(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.detachView();
        bind.unbind();
    }

    @Override
    public void showLoading(boolean show) {
        progress.animate().alpha(show ? 1 : 0).setDuration(250);
    }

    @Override
    public void showErrorLoading() {
    }

    @Override
    public void showItems(List<Video> data) {
        adapter.setItems(data);
    }

    @Override
    public void showEmpty() {
    }

    @Override
    public void hideEmpty() {
    }

    @Override
    public void setTextSize(float textSize) {
        adapter.setTextSize(textSize);
    }
}
