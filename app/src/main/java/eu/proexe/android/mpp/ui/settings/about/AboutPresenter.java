package eu.proexe.android.mpp.ui.settings.about;

import javax.inject.Inject;

import eu.proexe.android.mpp.data.DataManager;
import eu.proexe.android.mpp.ui.base.BasePresenter;
import eu.proexe.android.mpp.util.RxEventBus;

/**
 * Created by Michał Trętowicz  on 12.03.17.
 */

public class AboutPresenter extends BasePresenter<AboutMvpView> {

    @Inject
    public AboutPresenter(DataManager mDataManager, RxEventBus mRxEventBus) {
        super(mDataManager, mRxEventBus);
    }

    @Override
    public void attachView(AboutMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }
}
