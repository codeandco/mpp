package eu.proexe.android.mpp.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Michał Trętowicz  on 10.02.17.
 */

public class ListUtil {

    public static <T> List<List<T>> chopped(List<T> list, final int l) {
        List<List<T>> parts = new ArrayList<>();
        final int N = list.size();
        for (int i = 0; i < N; i += l) {
            parts.add(new ArrayList<>(
                    list.subList(i, Math.min(N, i + l)))
            );
        }
        return parts;
    }


}
