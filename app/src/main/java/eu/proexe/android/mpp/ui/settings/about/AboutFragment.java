package eu.proexe.android.mpp.ui.settings.about;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import eu.proexe.android.mpp.R;
import eu.proexe.android.mpp.ui.base.BaseActivity;

/**
 * Created by Michał Trętowicz  on 12.03.17.
 */

public class AboutFragment extends Fragment implements AboutMvpView {

    @Inject
    AboutPresenter presenter;

    @BindView(R.id.scroll)
    ScrollView scrollView;

    @BindView(R.id.text)
    HtmlTextView text;

    private Unbinder bind;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflated = inflater.inflate(R.layout.fragment_about, container, false);
        bind = ButterKnife.bind(this, inflated);

        text.setHtml("MPP-ME is a rapidly expanding, multi-faceted media company that offers creative media solutions to the luxury watch\n" +
                "\n" +
                "and jewellery industry in the Middle East.<br><br>" +
                "\n" +
                "Its established portfolio of publications includes flagship title Arabian Watches &amp; Jewellery Magazine, of which there\n" +
                "\n" +
                "are three independent editions: AWJ International, AWJ Kuwait and AWJ Levant.<br><br>" +
                "\n" +
                "MPP-ME was the first in the Middle East to launch a digitally distributed industry e-newsletter, which now has over\n" +
                "\n" +
                "24,000 subscribers via www.mpp-me.com - the region’s leading watch and jewellery news website.<br><br><br><br>" +
                "\n" +
                "<b>The company publishes various other titles and offers corporate publishing services:</b><br><br>" +
                "\n" +
                "* Jewels of Arabia - annual fine jewellery coffee table book<br><br>" +
                "\n" +
                "* Times of Arabia - annual horology coffee table book<br><br>" +
                "\n" +
                "* PENME - the only high-end writing instrument magazine in the Middle East<br><br>" +
                "\n" +
                "* Official publishing partner of Jewellery Arabia exhibition, Bahrain &amp; Doha Jewellery &amp; Watch exhibition, Qatar<br><br>" +
                "\n" +
                "* World of Luxury, Asia Jewellers Bahrain magazine &amp; Lahazaat, Trafalgar Kuwait magazine<br><br><br>" +
                "\n" +
                "<b>MPP-ME is also responsible for some of the region’s most prominent watch &amp; jewellery events:</b><br><br>" +
                "\n" +
                "* Middle East Jewellery of the Year awards for jewellery and writing instruments, held annually in Bahrain<br><br>" +
                "\n" +
                "* Middle East Watch of the Year awards held annually in Dubai, UAE<br><br>" +
                "\n" +
                "* Salon des Grandes Complications exhibition, held annually in Dubai, UAE<br><br>" +
                "\n" +
                "* Salon des Grandes Complications exhibition in Saudi Arabia, to be debut in 2017<br><br>" +
                "\n" +
                "* Collector gatherings for watch and jewellery companies");

//        text.setText(Html.fromHtml(getString(R.string.about), Html.FROM_HTML_MODE_COMPACT), TextView.BufferType.SPANNABLE);
        return inflated;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter.attachView(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((BaseActivity) getActivity()).activityComponent().inject(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        bind.unbind();
        presenter.detachView();
    }

    @OnClick(R.id.go_up)
    void goUp() {
        scrollView.fullScroll(ScrollView.FOCUS_UP);
    }

    @Override
    public void setTextSize(float textSize) {
        text.setTextSize(textSize);
    }
}
