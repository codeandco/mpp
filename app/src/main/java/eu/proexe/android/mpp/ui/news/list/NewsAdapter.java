package eu.proexe.android.mpp.ui.news.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jakewharton.rxrelay.PublishRelay;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.proexe.android.mpp.R;
import eu.proexe.android.mpp.data.ImageLoader;
import eu.proexe.android.mpp.data.local.PreferencesHelper;
import eu.proexe.android.mpp.data.model.response.Article;
import eu.proexe.android.mpp.data.remote.ApiService;
import eu.proexe.android.mpp.injection.ActivityContext;
import rx.Observable;
import timber.log.Timber;


public class NewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private static final int VIEW_HEADER = 0;
    private static final int VIEW_NORMAL = 1;
    private final LayoutInflater layoutInflater;
    private final ImageLoader imageLoader;

    private List<Article> items = new ArrayList<>();
    private String newsTypeIdExtra;
    private PublishRelay<Article> articlePublishRelay = PublishRelay.create();

    private float textSize = PreferencesHelper.DEFAULT_TEXT_SIZE;

    public void setTextSize(float textSize) {
        this.textSize = textSize;
        notifyDataSetChanged();
    }

    @Inject
    public NewsAdapter(@ActivityContext LayoutInflater layoutInflater, ImageLoader imageLoader) {
        this.layoutInflater = layoutInflater;
        this.imageLoader = imageLoader;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return viewType == VIEW_HEADER ?
                new HeaderHolder(layoutInflater.inflate(R.layout.item_news_header, parent, false)) :
                new ViewHolder(layoutInflater.inflate(R.layout.item_news, parent, false));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setItems(List<Article> items, String newsTypeIdExtra) {
        this.items = items;
        this.newsTypeIdExtra = newsTypeIdExtra;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (getItemViewType(position)) {


            case VIEW_HEADER:
                ((HeaderHolder) holder).setData(items.get(position));
                break;


            case VIEW_NORMAL:
                ((ViewHolder) holder).setData(items.get(position));
                break;
        }


    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? VIEW_HEADER : VIEW_NORMAL;
    }

    public Observable<Article> articleClicked() {
        return articlePublishRelay.asObservable();
    }

    class HeaderHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.text)
        TextView text;
        private Article item;


        HeaderHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        public void setData(Article item) {
            this.item = item;

            text.setTextSize(textSize);
            String url = String.format(ApiService.IMAGE_URL, newsTypeIdExtra) +
                    item.getImageName();
            imageLoader.load(image, url);
            text.setText(item.getHeader());
        }


        @Override
        public void onClick(View v) {
            articlePublishRelay.call(item);
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        public Article item;
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.text)
        TextView text;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        public void setData(Article item) {
            this.item = item;
            text.setTextSize(textSize);
            Timber.d("item.getHeader()  " + item.getHeader() + "   " +  item.getImageFullPath());

            String url = String.format(ApiService.IMAGE_URL,
                    newsTypeIdExtra) + item.getImageName();

//            if(!TextUtils.isEmpty(item.getImageFullPath())  ) {
                url = item.getImageFullPath();
//            }

            imageLoader.load(image, url);
            text.setText(item.getHeader());

        }

        @Override
        public void onClick(View v) {
            articlePublishRelay.call(item);
        }
    }


}
                                