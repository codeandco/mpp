package eu.proexe.android.mpp.ui.videos;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import eu.proexe.android.mpp.R;
import eu.proexe.android.mpp.data.ImageLoader;
import eu.proexe.android.mpp.data.local.PreferencesHelper;
import eu.proexe.android.mpp.data.model.response.video.Video;
import eu.proexe.android.mpp.injection.ActivityContext;
import eu.proexe.android.mpp.util.IntentUtils;
import eu.proexe.android.mpp.util.ListUtil;


public class VideosAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private static final int ITEM_HEADER = 0;
    private static final int ITEM_NORMAL = 1;
    private final LayoutInflater layoutInflater;
    private final ImageLoader imageLoader;

    private List<List<Video>> items = new ArrayList<>();
    private Video header;

    @Inject
    public VideosAdapter(@ActivityContext LayoutInflater layoutInflater,
                         ImageLoader imageLoader) {
        this.layoutInflater = layoutInflater;
        this.imageLoader = imageLoader;
    }

    private float textSize = PreferencesHelper.DEFAULT_TEXT_SIZE;

    public void setTextSize(float textSize) {
        this.textSize = textSize;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return viewType == ITEM_HEADER ?
                new HeaderViewHolder(
                        layoutInflater.inflate(R.layout.item_tv_header, parent, false)) :
                new ViewHolder(layoutInflater.inflate(R.layout.item_tv, parent, false));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setItems(List<Video> items) {
        header = items.get(0);
        this.items = ListUtil.chopped(items.subList(1, items.size()), 2);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case ITEM_HEADER:
                ((HeaderViewHolder) holder).setData(header);
                break;

            case ITEM_NORMAL:
                ((ViewHolder) holder).setData(items.get(position));
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? ITEM_HEADER : ITEM_NORMAL;
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        @BindView(R.id.image)
        ImageView image;

        @BindView(R.id.text)
        TextView text;

        @BindView(R.id.header_text)
        TextView headerText;

        @BindView(R.id.time)
        TextView time;

        Video item;

        HeaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        public void setData(Video item) {
            this.item = item;
            headerText.setText(item.Heading);
            text.setText(item.Description);

            imageLoader.load(image, item.imageName);

            this.text.setTextSize(textSize);
            this.headerText.setTextSize(textSize);
        }

        @Override
        public void onClick(View v) {
            if (item != null) {
                IntentUtils.openWebsite(itemView.getContext(), item.VideoName);
            }
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image1)
        ImageView image1;

        @BindView(R.id.text1)
        TextView text1;

        @BindView(R.id.image2)
        ImageView image2;

        @BindView(R.id.text2)
        TextView text2;

        Video item1;
        Video item2;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


        @OnClick({R.id.text1, R.id.image1})
        void item1Clicked() {
            if (item1 != null) {
                IntentUtils.openWebsite(itemView.getContext(), item2.VideoName);
            }
        }

        @OnClick({R.id.text2, R.id.image2})
        void item2Clicked() {
            if (item2 != null) {
                IntentUtils.openWebsite(itemView.getContext(), item2.VideoName);
            }
        }

        public void setData(List<Video> videos) {
            this.item1 = !videos.isEmpty() ? videos.get(0) : null;
            this.item2 = videos.size() == 2 ? videos.get(1) : null;

            this.text1.setTextSize(textSize);
            this.text2.setTextSize(textSize);

            if (this.item1 != null) {

                imageLoader.load(image1, item1.imageName);
                text1.setText(item1.Heading);
            }

            if (this.item2 != null) {
                imageLoader.load(image2, item2.imageName);
                text2.setText(item2.Heading);
            }
        }
    }
}
                                