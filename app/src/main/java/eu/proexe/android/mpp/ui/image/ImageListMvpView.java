package eu.proexe.android.mpp.ui.image;

import java.util.List;

import eu.proexe.android.mpp.data.event.EventTabs;
import eu.proexe.android.mpp.data.model.response.album.AlbumItem;
import eu.proexe.android.mpp.ui.base.LoadingMvpView;
import rx.Observable;

/**
 * Created by Michał Trętowicz  on 09.02.17.
 */

public interface ImageListMvpView extends LoadingMvpView<List<AlbumItem>> {

    Observable<AlbumItem> itemClicked();

    void showDetails(AlbumItem albumItem);

    void closeAlbumDetail();

    void showAdditionalTabs(EventTabs eventTabs);
}
