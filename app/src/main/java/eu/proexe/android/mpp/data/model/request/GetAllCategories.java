package eu.proexe.android.mpp.data.model.request;

import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

/**
 * Created by Michał Trętowicz  on 09.02.17.
 */
@Namespace(reference = "http://tempuri.org/")
@Root(name = "GetAllCategories")
public class GetAllCategories {
}
