package eu.proexe.android.mpp.ui.settings.terms;

import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import eu.proexe.android.mpp.R;
import eu.proexe.android.mpp.ui.base.BaseActivity;

/**
 * Created by Michał Trętowicz  on 12.03.17.
 */
public class TermsFragment extends Fragment implements TermsMvpView {

    @Inject
    TermsPresenter presenter;

    @BindView(R.id.scroll)
    ScrollView scrollView;

    @BindView(R.id.text)
    TextView text;

    private Unbinder bind;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflated = inflater.inflate(R.layout.fragment_terms, container, false);
        bind = ButterKnife.bind(this, inflated);
        return inflated;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.attachView(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((BaseActivity) getActivity()).activityComponent().inject(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        bind.unbind();
        presenter.detachView();
    }

    @OnClick(R.id.go_up)
    void goUp() {
        scrollView.fullScroll(ScrollView.FOCUS_UP);
    }

    @Override
    public void setTextSize(float textSize) {
        text.setTextSize(textSize);
        text.post(() -> goUpArrow.post(this::showOrHideArrow));
    }


    @BindView(R.id.go_up)
    View goUpArrow;

    private void showOrHideArrow() {
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int height = size.y;

        if (goUpArrow.getY() < height) {
            goUpArrow.setVisibility(View.INVISIBLE);
        } else {
            goUpArrow.setVisibility(View.VISIBLE);
        }
    }

}
