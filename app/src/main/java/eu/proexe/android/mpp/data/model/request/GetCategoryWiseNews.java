package eu.proexe.android.mpp.data.model.request;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "GetCategoryWiseNews")
public class GetCategoryWiseNews {

    @Element(name = "strNewsTypeID")
    private String strNewsTypeID;

    @Attribute(name = "xmlns")
    private String xmlns;

    public String getStrNewsTypeID() {
        return strNewsTypeID;
    }

    public void setStrNewsTypeID(String strNewsTypeID) {
        this.strNewsTypeID = strNewsTypeID;
    }

    public String getXmlns() {
        return xmlns;
    }

    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }

    @Override
    public String toString() {
        return "ClassPojo [strNewsTypeID = " + strNewsTypeID + ", xmlns = " + xmlns + "]";
    }
}