package eu.proexe.android.mpp.injection.component;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import eu.proexe.android.mpp.data.DataManager;
import eu.proexe.android.mpp.data.ImageLoader;
import eu.proexe.android.mpp.data.local.PreferencesHelper;
import eu.proexe.android.mpp.data.remote.ApiService;
import eu.proexe.android.mpp.injection.ApplicationContext;
import eu.proexe.android.mpp.injection.module.ApiModule;
import eu.proexe.android.mpp.injection.module.ApplicationModule;
import eu.proexe.android.mpp.util.RxEventBus;

@Singleton
@Component(modules = {ApplicationModule.class, ApiModule.class})
public interface ApplicationComponent {

    @ApplicationContext
    Context context();

    Application application();

    ApiService apiService();

    PreferencesHelper preferencesHelper();

    DataManager dataManager();

    RxEventBus eventBus();

    ImageLoader imageLoader();

}
