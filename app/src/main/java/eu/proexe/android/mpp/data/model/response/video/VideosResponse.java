package eu.proexe.android.mpp.data.model.response.video;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Michał Trętowicz  on 09.02.17.
 */

@Root(name = "soap:Envelope")
public class VideosResponse {

    @Path("Body/GetVideosResponse/GetVideosResult/diffgram/NewDataSet")
    @ElementList(inline = true)
    private List<Video> videos;

    public List<Video> getVideos() {
        return videos;
    }
}
