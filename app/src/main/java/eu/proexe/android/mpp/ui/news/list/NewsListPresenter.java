package eu.proexe.android.mpp.ui.news.list;


import java.util.List;

import javax.inject.Inject;

import eu.proexe.android.mpp.data.DataManager;
import eu.proexe.android.mpp.data.model.response.Article;
import eu.proexe.android.mpp.ui.base.LoadingPresenter;
import eu.proexe.android.mpp.util.RxEventBus;
import eu.proexe.android.mpp.util.RxUtil;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

//@ConfigPersistent
public class NewsListPresenter extends LoadingPresenter<NewsListMvpView> {

    private Subscription subscription;
    private Subscription itemClickSub;

    @Inject
    public NewsListPresenter(DataManager mDataManager, RxEventBus mRxEventBus) {
        super(mDataManager, mRxEventBus);
    }

    @Override
    public void attachView(NewsListMvpView mvpView) {
        super.attachView(mvpView);

        RxUtil.unsubscribe(itemClickSub);

        itemClickSub = getMvpView().articleClicked()
                .subscribe(new Subscriber<Article>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(Article article) {
                        getMvpView().showArticleDetail(article);
                    }
                });
    }

    public void loadArticles(String newsTypeId) {
        RxUtil.unsubscribe(subscription);
        subscription = mDataManager.getArticles(newsTypeId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new LoadingSubscriber<List<Article>>());
    }

    @Override
    public void detachView() {
        super.detachView();
        RxUtil.unsubscribe(subscription);
        RxUtil.unsubscribe(itemClickSub);
    }
}
