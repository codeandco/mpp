package eu.proexe.android.mpp.ui.settings;

import javax.inject.Inject;

import eu.proexe.android.mpp.data.DataManager;
import eu.proexe.android.mpp.ui.base.BasePresenter;
import eu.proexe.android.mpp.util.RxEventBus;

/**
 * Created by Michał Trętowicz  on 19.03.17.
 */

public class SettingsPresenter extends BasePresenter<SettingsMvpView> {

    @Inject
    public SettingsPresenter(DataManager mDataManager, RxEventBus mRxEventBus) {
        super(mDataManager, mRxEventBus);
    }
}
