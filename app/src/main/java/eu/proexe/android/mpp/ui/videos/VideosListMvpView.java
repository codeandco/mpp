package eu.proexe.android.mpp.ui.videos;

import java.util.List;

import eu.proexe.android.mpp.data.model.response.video.Video;
import eu.proexe.android.mpp.ui.base.LoadingMvpView;


public interface VideosListMvpView extends LoadingMvpView<List<Video>> {
}
