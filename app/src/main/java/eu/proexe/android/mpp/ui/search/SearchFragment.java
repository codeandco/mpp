package eu.proexe.android.mpp.ui.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;

import com.jakewharton.rxbinding.support.v7.widget.RxSearchView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import eu.proexe.android.mpp.R;
import eu.proexe.android.mpp.data.model.response.Article;
import eu.proexe.android.mpp.ui.base.BaseActivity;
import eu.proexe.android.mpp.ui.news.details.NewsDetailsActivity;
import eu.proexe.android.mpp.util.KeyboardUtils;
import rx.Observable;

/**
 * Created by Michał Trętowicz  on 09.02.17.
 */

public class SearchFragment extends Fragment implements SearchMvpView {

    private Unbinder bind;

    public static SearchFragment newInstance() {
        SearchFragment fragment = new SearchFragment();
        fragment.setArguments(new Bundle());
        return fragment;
    }

    @BindView(R.id.search)
    SearchView searchView;

    @Inject
    SearchAdapter newsAdapter;

    @Inject
    SearchPresenter presenter;

    @BindView(R.id.list)
    RecyclerView list;

    @BindView(R.id.progress)
    View progress;

    @BindView(R.id.go)
    Button goBtn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflated = inflater.inflate(R.layout.fragment_search, container, false);
        bind = ButterKnife.bind(this, inflated);

        progress.setAlpha(0);

//        searchView.setOnCloseListener(() -> {
//            KeyboardUtils.hideKeyboard(getActivity());
//            searchView.onActionViewCollapsed();
//            return true;
//        });



//        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                goClicked();
//                return true;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//                return false;
//            }
//        });

        list.setAdapter(newsAdapter);

        EditText searchEditText = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.white));
        searchEditText.setHintTextColor(getResources().getColor(R.color.white));


        searchEditText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                goClicked();
                return true;
            }
            else {
                return false;
            }
        });


        RxSearchView.queryTextChanges(searchView).subscribe(presenter.getSearchProxy());

        return inflated;
    }



    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter.attachView(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((BaseActivity) getActivity()).activityComponent().inject(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.detachView();
        bind.unbind();
    }

    @Override
    public void showLoading(boolean show) {
        progress.animate().alpha(show ? 1 : 0).setDuration(250);
    }

    @Override
    public void showErrorLoading() {
    }

    @Override
    public void showItems(List<Article> data) {
        newsAdapter.setItems(data, "");
    }

    @Override
    public void showEmpty() {

    }

    @Override
    public void hideEmpty() {

    }

    @Override
    public Observable<Article> onItemClicked() {
        return newsAdapter.articleClicked();
    }

    @Override
    public void showDetail(Article article) {
        NewsDetailsActivity.start(getActivity(), article, "", article.getImageFullPath(), true);
    }

    @OnClick(R.id.go)
    void goClicked() {

        if(!TextUtils.isEmpty(searchView.getQuery().toString())) {
            presenter.loadItems(searchView.getQuery().toString());
        }
        KeyboardUtils.hideKeyboard(getActivity());
    }

    @OnClick(R.id.action_search)
    void searchClicked() {
        searchView.requestFocus();
        KeyboardUtils.showKeyboard(getActivity());
    }

    @Override
    public void setTextSize(float textSize) {
        goBtn.setTextSize(textSize);
    }
}
