package eu.proexe.android.mpp.ui.videos;


import javax.inject.Inject;

import eu.proexe.android.mpp.data.DataManager;
import eu.proexe.android.mpp.injection.ConfigPersistent;
import eu.proexe.android.mpp.ui.base.LoadingPresenter;
import eu.proexe.android.mpp.util.RxEventBus;
import eu.proexe.android.mpp.util.RxUtil;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@ConfigPersistent
public class VideosListPresenter extends LoadingPresenter<VideosListMvpView> {

    private Subscription subscription;

    @Inject
    public VideosListPresenter(DataManager mDataManager, RxEventBus mRxEventBus) {
        super(mDataManager, mRxEventBus);
    }

    @Override
    public void attachView(VideosListMvpView mvpView) {
        super.attachView(mvpView);

        RxUtil.unsubscribe(subscription);

        subscription = mDataManager.getVideos()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new LoadingSubscriber<>());
    }

    @Override
    public void detachView() {
        super.detachView();
        RxUtil.unsubscribe(subscription);
    }
}
