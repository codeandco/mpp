package eu.proexe.android.mpp.data.event;

import android.content.Intent;

/**
 * Created by Michał Trętowicz  on 15.03.17.
 */

public  class OnActivityResultEvent {

    private final int requestCode;
    private final int resultCode;
    private final Intent data;

    public OnActivityResultEvent(int requestCode, int resultCode, Intent data) {
        this.requestCode = requestCode;
        this.resultCode = resultCode;
        this.data = data;
    }

    public Intent getData() {
        return data;
    }

    public int getRequestCode() {
        return requestCode;
    }

    public int getResultCode() {
        return resultCode;
    }
}
