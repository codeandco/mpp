package eu.proexe.android.mpp.ui.news;

import eu.proexe.android.mpp.data.model.response.category.NewsCategory;
import eu.proexe.android.mpp.ui.base.MvpView;

/**
 * Created by Michał Trętowicz  on 18.03.17.
 */

public interface NewsCategoriesMvpView extends MvpView {
    void showTabWithName(NewsCategory name);
}
