package eu.proexe.android.mpp.ui.settings.notifications_2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.Unbinder;
import eu.proexe.android.mpp.R;
import eu.proexe.android.mpp.ui.base.BaseActivity;


public class NotiFragment extends Fragment implements NotiMvpView {



    @Inject
    NotiPresenter presenter;


    private Unbinder bind;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflated = inflater.inflate(R.layout.fragment_settings_notifications, container, false);
        bind = ButterKnife.bind(this, inflated);



        return inflated;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter.attachView(this);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((BaseActivity) getActivity()).activityComponent().inject(this);
    }




    @Override
    public void onDestroyView() {
        super.onDestroyView();
        bind.unbind();
        presenter.detachView();
    }



    @OnCheckedChanged(R.id.notifications_at_night)
    void notificationsAtNightChanged(boolean checked) {

//        presenter.notificationsAtNightChanged(checked);
    }

    @BindViews({R.id.title, R.id.subtitle})
    List<TextView> views;

    @Override
    public void setTextSize(float textSize) {
        for (TextView view : views) {
            view.setTextSize(textSize);
        }
    }




}
