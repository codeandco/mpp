package eu.proexe.android.mpp.ui.settings.text.size;

import eu.proexe.android.mpp.ui.base.MvpView;

/**
 * Created by Michał Trętowicz  on 12.03.17.
 */

public interface TextSizeSettingsMvpView extends MvpView {
    void showUseSystemSize(boolean useSystemSize);

    void showTextSize(float textSize);

    void showProgress(int progress);
}
