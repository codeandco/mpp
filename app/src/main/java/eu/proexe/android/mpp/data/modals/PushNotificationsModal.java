package eu.proexe.android.mpp.data.modals;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Shoeb on 3/7/17.
 */

public class PushNotificationsModal implements Serializable {
    public String getPkey() {
        return pkey;
    }

    public void setPkey(String pkey) {
        this.pkey = pkey;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName("pkey")
    private String pkey = "";

    public String getNewsTypeId() {
        return newsTypeId;
    }

    public void setNewsTypeId(String newsTypeId) {
        this.newsTypeId = newsTypeId;
    }

    @SerializedName("newsTypeId")
    private String newsTypeId = "";

    @SerializedName("message")
    private String message = "";
}
